﻿#pragma checksum "D:\New folder\Baija-dev\Baija-dev\Views\MainPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "7F721CD5F2AEC9C9797F244547D30296"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Baija_dev {
    
    
    public partial class MainPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Shapes.Rectangle baijaLoginBtn;
        
        internal System.Windows.Controls.Button registerBtn;
        
        internal System.Windows.Controls.Grid loginFields;
        
        internal Microsoft.Phone.Controls.PhoneTextBox loginEmail;
        
        internal System.Windows.Controls.PasswordBox loginPassword;
        
        internal System.Windows.Controls.Button loginButton;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Baija-dev;component/Views/MainPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.baijaLoginBtn = ((System.Windows.Shapes.Rectangle)(this.FindName("baijaLoginBtn")));
            this.registerBtn = ((System.Windows.Controls.Button)(this.FindName("registerBtn")));
            this.loginFields = ((System.Windows.Controls.Grid)(this.FindName("loginFields")));
            this.loginEmail = ((Microsoft.Phone.Controls.PhoneTextBox)(this.FindName("loginEmail")));
            this.loginPassword = ((System.Windows.Controls.PasswordBox)(this.FindName("loginPassword")));
            this.loginButton = ((System.Windows.Controls.Button)(this.FindName("loginButton")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
        }
    }
}

