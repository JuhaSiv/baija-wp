﻿#pragma checksum "D:\New folder\Baija-dev\Baija-dev\OwnProductPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "89A117DCC414EB964D0ECD75761DEF44"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Baija_dev {
    
    
    public partial class OwnProductPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Grid NavBar;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.Grid productInfoGrid;
        
        internal System.Windows.Controls.TextBlock Description_block;
        
        internal System.Windows.Controls.TextBlock Hashtag_block;
        
        internal System.Windows.Controls.Image DeleteOwnSale;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Baija-dev;component/OwnProductPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.NavBar = ((System.Windows.Controls.Grid)(this.FindName("NavBar")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.productInfoGrid = ((System.Windows.Controls.Grid)(this.FindName("productInfoGrid")));
            this.Description_block = ((System.Windows.Controls.TextBlock)(this.FindName("Description_block")));
            this.Hashtag_block = ((System.Windows.Controls.TextBlock)(this.FindName("Hashtag_block")));
            this.DeleteOwnSale = ((System.Windows.Controls.Image)(this.FindName("DeleteOwnSale")));
        }
    }
}

