﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Net;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Baija_dev;
using System.Diagnostics;

namespace Swipe
{
   public class SwipeContentControl : ContentControl
    {
       
       public event EventHandler SwipeLeft;
       protected virtual void OnSwipeLeft()
       {
           Debug.WriteLine("swipeleft");
           if(this.SwipeLeft != null)
           {
               this.SwipeLeft(this, new EventArgs());
              
           }
       }
       
       public event EventHandler SwipeRight;
       protected virtual void OnSwipeRight()
       {
           Debug.WriteLine("swiperight");
           if (this.SwipeRight != null)
           {
               this.SwipeRight(this, new EventArgs());
           }
       }

       public static readonly DependencyProperty CanSwipeLeftProperty = 
           DependencyProperty.Register("CanSwipeLeft", typeof(bool), typeof(SwipeContentControl), new PropertyMetadata(false));

       public bool CanSwipeLeft
       {
           get { return (bool)GetValue(CanSwipeLeftProperty); }
           set { SetValue(CanSwipeLeftProperty, value); }
       }

       public static readonly DependencyProperty CanSwipeRightProperty =
           DependencyProperty.Register("CanSwipeRight", typeof(bool), typeof(SwipeContentControl), new PropertyMetadata(false));

       public bool CanSwipeRight
       {
           get { return (bool)GetValue(CanSwipeRightProperty); }
           set { SetValue(CanSwipeRightProperty, value); }
       }

       public SwipeContentControl()
       {
           this.RenderTransform = new TranslateTransform();

           this.DefaultStyleKey = typeof(ContentControl);

           GestureListener listener = GestureService.GetGestureListener(this);

           listener.DragDelta += DragDelta;
           listener.DragCompleted += DragCompleted;
       }

       private void DragDelta (object sender, Microsoft.Phone.Controls.DragDeltaGestureEventArgs e)
       {
           if(((e.HorizontalChange == 0) && this.CanSwipeRight))
           {
               TranslateTransform transform = (TranslateTransform)this.RenderTransform;

               //Check if the control is being dragged
               if(transform.X == 0)
               {
                   if((e.HorizontalChange > -15.0d) && (e.HorizontalChange <15.0d))
                   {
                       //The change is too small to start moving the control

                       return;
                   }
                   transform.X += e.HorizontalChange;
               }

           }

       }
      /* public static void Show(this UIElement element, int duration)
       {
           VisualExtensions.Show(element,duration,null);

       }
       public static void Animate (this UIElement element, string property, int duration, double value, Action callback)
       {
           Storyboard storyBoard = new Storyboard();

           DoubleAnimation animation = new DoubleAnimation();
           animation.To = value;
           animation.Duration = new Duration(TimeSpan.FromMilliseconds(duration));

           if(callback != null)
           {
               animation.Completed += delegate { callback(element); };
           }

           Storyboard.SetTarget(animation,element);
           Storyboard.SetTargetProperty(animation, new PropertyPath(property));

           storyBoard.Children.Add(animation);
           storyBoard.Begin();

       }*/

  /*     private void DragCompleted(object sender,Microsoft.Phone.Controls.DragCompletedGestureEventArgs e)
       {
           TranslateTransform transform = (TranslateTransform)this.RenderTransform;
           if((transform.X > 120.0d) && this.CanSwipeRight)
           {
               //Calculate the offset
               double offset = this.ActualWidth + 20.0d;

               //Fadeout and shift to the right
               this.Animate("Opacity",100,0.0d);
               this.Animate("(RenderTransform).(TranslateTransform.X)",100, offset,(element) =>
               {
                   //Raise the event so the containing page can set the new content
                   this.OnSwipeRight();

                   //Delay 100m with an 'empty' animation for a smooth animation
                   this.Animate("(RenderTransform).(TranslateTransform.X)", 100, offset, (element2) =>
                   {
                               /* Move the control to the left hand side - we need to retrieve the 
                         * transform again here as it no longer points to the same object. I
                         * haven't figured out if there is a reason for this or if it is a bug. */
                    /*       transform = (TranslateTransform)this.RenderTransform;
                           transform .X = -offset;
                           //Fade in and slide from the left 
                           this.Animate("Opacity",100,1.0d);
                           this.Animate("(RenderTransform).(TranslateTransform.X)", 150, 0.0d);                   
                    });
                });
           }
           else if ((transform.X <-120.0d) && this.CanSwipeLeft)
           {
                double offset = this.ActualWidth + 20.0d;
                Debug.WriteLine("test");
               //Fadeout and shift to the right
               this.Animate("Opacity",100,0.0d);
               this.Animate("(RenderTransform).(TranslateTransform.X)",100, -offset,(element) =>
               {
                   //Raise the event so the containing page can set the new content
                   this.OnSwipeRight();
                   //Delay 100m with an 'empty' animation for a smooth animation
                   this.Animate("(RenderTransform).(TranslateTransform.X)", 100, -offset, (element2) =>
                       {
                               /* Move the control to the left hand side - we need to retrieve the 
                         * transform again here as it no longer points to the same object. I
                         * haven't figured out if there is a reason for this or if it is a bug. */
                     /*      transform = (TranslateTransform)this.RenderTransform;
                     /*      transform .X = -offset;
                           //Fade in and slide from the left 
                           this.Animate("Opacity",100,1.0d);
                           this.Animate("(RenderTransform).(TranslateTransform.X)", 150, 0.0d);                   
                      });
            });

           }
           else if (transform.X != 0.0d)
           {
               this.Animate("(RenderTransform).(TranslateTRansform.X)",200,0.0d);
           }
       }
                

private void Animate(string p1,int p2,double p3)
{
 	throw new NotImplementedException();
}


    }
}*/
