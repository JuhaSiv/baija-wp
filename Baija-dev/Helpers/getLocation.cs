﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Device.Location;
using System.Diagnostics;

namespace Baija_dev
{
    class getLocation
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        GeoCoordinateWatcher watcher;
        public getLocation()
        {
            this.watcher = new GeoCoordinateWatcher();
            this.watcher.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(watcher_PositionChanged);
            bool started = this.watcher.TryStart(false, TimeSpan.FromMilliseconds(2000));
            if (!started)
            {
                Console.WriteLine("GeoCoordinateWatcher timed out on start.");
            }

             GeoCoordinate coord = watcher.Position.Location;
             Longitude = coord.Longitude.ToString();
             Latitude = coord.Latitude.ToString();
        }


        void watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            PrintPosition(e.Position.Location.Latitude, e.Position.Location.Longitude);
        }

        void PrintPosition(double Latitude_, double Longitude_)
        {
            Longitude = Longitude_.ToString();
            Latitude = Latitude_.ToString();
            Debug.WriteLine("Latitude: {0}, Longitude {1}", Latitude, Longitude);
        }
    }
}
