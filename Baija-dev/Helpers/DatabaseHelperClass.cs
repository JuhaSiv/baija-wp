﻿using Baija_dev.dbClasses;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Baija_dev
{
        //This class for perform all database CRUD operations 
        public class DatabaseHelperClass
        {
            SQLiteConnection dbConn;

            //Create Tabble 
            public async Task<bool> onCreate(string DB_PATH)
            {
                try
                {
                    if (!CheckFileExists(DB_PATH).Result)
                    {
                        using (dbConn = new SQLiteConnection(DB_PATH))
                        {
                            dbConn.CreateTable<ProductsTable>();
                            dbConn.CreateTable<ImagesTable>();
                            dbConn.CreateTable<ChatTable>();
                    
                        }
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            private async Task<bool> CheckFileExists(string fileName)
            {
                try
                {
                    var store = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            // Retrieve the specific contact from the database. 
 

            //**********************************IMAGE FUNCTIONS********************************************************************************
            public ImagesTable getFirstProductImage(string mongoId)
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    ImagesTable firstPicture = dbConn.Query<ImagesTable>("SELECT * FROM ImagesTable WHERE saleId_ ='" + mongoId + "'").FirstOrDefault();

                    return firstPicture;
                }
            }
            public ObservableCollection<ImagesTable> getAllPictures()
            {
                List<ImagesTable> picList = new List<ImagesTable>();

                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {

                    List<ImagesTable> myCollection = dbConn.Query<ImagesTable>("SELECT * FROM ImagesTable");
                    ObservableCollection<ImagesTable> Collection = new ObservableCollection<ImagesTable>(myCollection);
                    return Collection;
                }
            }

            public ObservableCollection<ImagesTable> getAllPictures(string mongoID)
            {
                List<ImagesTable> picList = new List<ImagesTable>();

                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {

                    List<ImagesTable> myCollection = dbConn.Query<ImagesTable>("SELECT * FROM ImagesTable WHERE mongoId_ ='" + mongoID + "'");
                    ObservableCollection<ImagesTable> Collection = new ObservableCollection<ImagesTable>(myCollection);
                    return Collection;
                }
            }

            public void InsertPicture(ImagesTable newImage)
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var query = dbConn.Query<ProductsTable>("select * from ImagesTable where mongoId_ ='" + newImage.mongoId_ + "'").FirstOrDefault();
                    if (query == null)
                    {
                        dbConn.RunInTransaction(() =>
                        {
                            dbConn.Insert(newImage);
                        });
                    }
                }
            }

            public ObservableCollection<ImagesTable> getAllPicturesOfProduct(string mongoID)
            {
                List<ImagesTable> picList = new List<ImagesTable>();

                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {

                    List<ImagesTable> myCollection = dbConn.Query<ImagesTable>("SELECT * FROM ImagesTable WHERE saleId_ ='" + mongoID + "'");
                    ObservableCollection<ImagesTable> Collection = new ObservableCollection<ImagesTable>(myCollection);
                    Debug.WriteLine("Collection count: "+Collection.Count);
                    return Collection;
                }
            }



//**********************************PRODUCT FUNCTIONS********************************************************************************



            //Get product by ID
            public ProductsTable getProduct(string mongoId)
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var existingProduct = dbConn.Query<ProductsTable>("SELECT * FROM ProductsTable WHERE mongoId_ ='" + mongoId + "'").FirstOrDefault();
                    return existingProduct;
                }
            }

            //Return True / False based on if the chosen product is favorited by the user
            public bool checkIfFav(string mongoId)
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var query = dbConn.Query<ProductsTable>("SELECT * FROM ProductsTable WHERE mongoId_ ='" + mongoId + "' AND type_='fav'").FirstOrDefault();
                    if (query != null)
                        return true;

                    //else
                    return false;
                }
            }
            public bool checkIfOwn(string mongoId)
            {
                Debug.WriteLine(mongoId);
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var query = dbConn.Query<ProductsTable>("SELECT * FROM ProductsTable WHERE mongoId_ ='" + mongoId + "' AND type_='own'").FirstOrDefault();
                    if (query != null)
                    {
                        Debug.WriteLine("IF");
                        return true;
                    }
                      

                    //else
                    return false;
                }
            }


            // Retrieve the all contact list from the database. 
            public ObservableCollection<ProductsTable> getAllProducts()
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    List<ProductsTable> myCollection = dbConn.Table<ProductsTable>().ToList<ProductsTable>();
                    ObservableCollection<ProductsTable> ContactsList = new ObservableCollection<ProductsTable>(myCollection);
                    return ContactsList;

                }
            }
            //Return a list of Own products
            public ObservableCollection<ProductsTable> getOwnProducts(string userID)
            {
                List<ProductsTable> picList = new List<ProductsTable>();

                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                   
                    List<ProductsTable> myCollection = dbConn.Query<ProductsTable>("SELECT * FROM ProductsTable WHERE userId_='" + userID + "' AND type_='own'");
                    ObservableCollection<ProductsTable> Collection = new ObservableCollection<ProductsTable>(myCollection);
                    Debug.WriteLine(Collection.Count + "");
                    return Collection;
                }
            }
            //Return list of Favorited products for the current user
            public ObservableCollection<ProductsTable> getFavProducts()
            {
                List<ProductsTable> picList = new List<ProductsTable>();

                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {

                    List<ProductsTable> myCollection = dbConn.Query<ProductsTable>("SELECT * FROM ProductsTable WHERE type_='fav'");
                    ObservableCollection<ProductsTable> Collection = new ObservableCollection<ProductsTable>(myCollection);
                    return Collection;
                }
            }
            public ObservableCollection<ProductsTable> getPreviousSearchResults()
            {
                List<ProductsTable> picList = new List<ProductsTable>();

                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {

                    List<ProductsTable> myCollection = dbConn.Query<ProductsTable>("SELECT * FROM ProductsTable WHERE type_ ='temp'");
                    ObservableCollection<ProductsTable> Collection = new ObservableCollection<ProductsTable>(myCollection);
                    return Collection;
                }
            }
            //Update existing Product
       /*    public void UpdateProduct(ProductsTable updatedProduct)
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var query = dbConn.Query<ProductsTable>("select * from ProductsTable where sqlId =" + updatedProduct.SqlId).FirstOrDefault();
                    if (query != null)
                    { 
                        query.title_ = updatedProduct.title_;
                        query.desc_ = updatedProduct.desc_;
                        query.price_ = updatedProduct.price_;
                        query.tags_ = updatedProduct.tags_;
                        dbConn.RunInTransaction(() =>
                        {
                            dbConn.Update(query);
                        });
                    }
                }
            }*/
            // Insert the new product in the products table. 
            public void Insert(ProductsTable newProduct,string type)
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                     var query = dbConn.Query<ProductsTable>("select * from ProductsTable where mongoId_ ='" + newProduct.mongoId_ + "' AND type_='"+type+"'" ).FirstOrDefault();
                    if (query == null)
                    {
                        dbConn.RunInTransaction(() =>
                        {
                            dbConn.Insert(newProduct);
                            Debug.WriteLine("INSERT");
                        });
                    }
                }
            }
      

            //Delete specific contact 
            public void DeleteTemps()
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var deleteProduct = dbConn.Query<ProductsTable>("Select * from ProductsTable where type_ = 'temp'");
                    if (deleteProduct != null)
                    {
                            dbConn.RunInTransaction(() =>
                            {
                                dbConn.Query<ProductsTable>("Delete From ProductsTable Where type_='temp'");
                            });
                        
                    }
                }
            }
            public void DeleteFavs()
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var deleteProduct = dbConn.Query<ProductsTable>("Select * from ProductsTable where type_ = 'fav'");
                    if (deleteProduct != null)
                    {
                        dbConn.RunInTransaction(() =>
                        {
                            dbConn.Delete(deleteProduct);
                        });
                    }
                }
            }
            public void DeleteOwns()
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var deleteProduct = dbConn.Query<ProductsTable>("select * from ProductsTable where type_ = 'own'");
                    if (deleteProduct != null)
                    {
                        dbConn.RunInTransaction(() =>
                        {
                            dbConn.Delete(deleteProduct);
                        });
                    }
                }
            }
            public void DeleteFromFav(string mongoId)
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var deleteProduct = dbConn.Query<ProductsTable>("Select * from ProductsTable where type_ = 'fav' AND mongoId_ = '" + mongoId + "'");
                    if (deleteProduct != null)
                    {
                        dbConn.RunInTransaction(() =>
                        {
                            dbConn.Query<ProductsTable>("Delete From ProductsTable Where mongoId_ ='" + mongoId + "' AND type_='fav'");
                        });
                    }
                }
            }
            public void DeleteFromOwn(string mongoId)
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var deleteProduct = dbConn.Query<ProductsTable>("Select * from ProductsTable where type_ = 'fav' AND mongoId_ = '" + mongoId + "'");
                    if (deleteProduct != null)
                    {
                        dbConn.RunInTransaction(() =>
                        {
                            dbConn.Query<ProductsTable>("Delete From ProductsTable Where mongoId_ ='" + mongoId + "' AND type_='own'");
                        });
                    }
                }
            }
            //Delete all products or delete Contacts table 
            public void ClearDataBase()
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                   var images = dbConn.Query<ImagesTable>("DELETE FROM ImagesTable WHERE mongoId_ != 0");
                   if (images != null)
                   {
                       foreach (ImagesTable del in images)
                       {
                           dbConn.RunInTransaction(() =>
                           {

                               dbConn.Delete(del);
                           });
                       }
                    
                   }

                   var products = dbConn.Query<ProductsTable>("DELETE FROM ProductsTable WHERE mongoId_ != 0");
                   if (products != null)
                   {
                       foreach (ProductsTable del in products)
                       {
                           dbConn.RunInTransaction(() =>
                           {

                               dbConn.Delete(del);
                           });
                       }

                   }
                   var Chats = dbConn.Query<ChatTable>("DELETE FROM ChatTable WHERE mongoId_ != 0");
                   if (Chats != null)
                   {
                       foreach (ChatTable del in Chats)
                       {
                           dbConn.RunInTransaction(() =>
                           {

                               dbConn.Delete(del);
                           });
                       }

                   }
                }
            }


            public void addFav(ProductsTable newProduct)
            {
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    var query = dbConn.Query<ProductsTable>("select * from ProductsTable where mongoId_ ='" + newProduct.mongoId_ + "' AND type_ ='fav'").FirstOrDefault();
                    if (query == null)
                    {
                        dbConn.RunInTransaction(() =>
                        {
                            dbConn.Insert(newProduct);
                            Debug.WriteLine("INSERT");
                        });
                    }
                }
            }
//******************************************************MESSAGE FUNCTIONS*********************************************************************************************************

            public void InsertChatMessage(ChatTable newMessage)
            {
         //       Debug.WriteLine("Insert call");
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
               //     Debug.WriteLine("using DBcon call");
                    var query = dbConn.Query<ChatTable>("select * from ChatTable where mongoId_ ='" + newMessage.mongoId_ + "'").FirstOrDefault();
               //     Debug.WriteLine(query);
                    if (query == null)
                    {
                     //   Debug.WriteLine("if null");
                        dbConn.RunInTransaction(() =>
                        {
                            dbConn.Insert(newMessage);
                   //         Debug.WriteLine("INSERT");
                        });
                    }
                }
            }


            public ObservableCollection<ChatTable> getAllMessagesForThisChat(string productID,string ownID,string partnerID)
            {
                List<ChatTable> picList = new List<ChatTable>();

                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {

                    List<ChatTable> myCollection = dbConn.Query<ChatTable>("SELECT * FROM ChatTable WHERE productId_ ='" + productID + "' AND ((receiverId_ = '" + ownID + "' AND senderId_ = '" + partnerID + "') OR (receiverId_ ='" + partnerID + "' AND senderId_ = '" + ownID + "'))");
                    ObservableCollection<ChatTable> Collection = new ObservableCollection<ChatTable>(myCollection);
                    return Collection;
                }
            }
            public ChatTable getChatObject(string chatId)
            {
                ChatTable newChatTable = new ChatTable();
                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {
                    newChatTable = dbConn.Query<ChatTable>("SELECT * FROM ChatTable WHERE chatId_ = '" + chatId + "'").FirstOrDefault();
                    return newChatTable;
                }
            }
            public ObservableCollection<ChatTable> getMyChats(string ownID)
            {
                List<ChatTable> picList = new List<ChatTable>();

                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {

                    List<ChatTable> myCollection = dbConn.Query<ChatTable>("SELECT * FROM ChatTable GROUP BY chatId_");
                    ObservableCollection<ChatTable> Collection = new ObservableCollection<ChatTable>(myCollection);
                    return Collection;
                }
            }
            public string getLatestMessageTimestamp()
            {
                ChatTable latestTimestamp = new ChatTable();

                using (var dbConn = new SQLiteConnection(App.DB_PATH))
                {

                    latestTimestamp = dbConn.Query<ChatTable>("SELECT * FROM ChatTable").LastOrDefault();
                }
                if (latestTimestamp != null){
                    return latestTimestamp.timestamp_;
                }
                return "";
             
            }
        }
 }
