﻿
using System; 
using System.Collections.Generic; 
using System.Collections.ObjectModel; 
using System.Linq; 
using System.Text; 
using System.Threading.Tasks; 

namespace Baija_dev.ViewModel
{
    class getAllProducts
    {
        DatabaseHelperClass db_helper = new DatabaseHelperClass();
        public ObservableCollection<ProductsTable> getAllProductsHelper()
        {
            return db_helper.getAllProducts();
        }
    }
}
