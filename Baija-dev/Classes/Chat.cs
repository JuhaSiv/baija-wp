﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Baija_dev
{
    public class Chat
    {
        public string chatPartnerName_ { get; set; }
        public string chatId_ { get; set; }
        public string productName_ { get; set; }
        public string productId_ { get; set; }
        public ImageSource previewImg_ { get; set; }


        public Chat()
        {

        }
        public Chat(string partner, string id,string product,ImageSource imgsrc)
        {
            chatPartnerName_ = partner;
            chatId_ = id;
            productName_ = product;
            previewImg_ = imgsrc;

        }
    }

    
}
