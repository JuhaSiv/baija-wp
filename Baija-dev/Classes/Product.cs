﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Baija_dev
{
   public class Product
    {
      
        
       public String Title_ { get; set; }
       public String Desc_ { get; set; }
       public String Hashtags_ { get; set; }
       public String Price_ { get; set; }
       public String Id_ { get; set; }
       public String SellerId_ { get; set; }
       public String SellerName_ { get; set; }
       public String Timestamp_ { get; set; }
       public String Type_ { get; set; }
       public String Distance_ { get; set; }
       public ImageSource previewImg_ { get; set; }

       public Product()
       {
           Title_ = "";
           Desc_ = "";
           Hashtags_ =  "";
           Price_ = "";
           Id_ = "";
           SellerId_ = "";
           SellerName_ = "";
           Timestamp_ = "";
           Type_ = "";
           Distance_ = "";
           previewImg_ = null;
       }

        public Product(String Title,String Desc,String Hashtags,String Price,String Id,String SellerId,String SellerName,String Timestamp,String Type,String Distance){

            Title_ = Title;
            Desc_  = Desc;
            Hashtags_ =Hashtags;
            Price_ = Price;
            Id_  = Id;
            SellerId_  = SellerId;
            SellerName_ = SellerName;
            Timestamp_ = Timestamp;
            Type_ = Type;
            Distance_ = Distance;
        }

    }
}
