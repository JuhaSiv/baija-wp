﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baija_dev
{
    class ProductJsonDataObject
    {
        public String title { get; set; }
        public String desc { get; set; }
        public String tags { get; set; }
        public String price { get; set; }
        public String userID { get; set; }
        public String Timestamp { get; set; }
        public String Type { get; set; }
        public String lon { get; set; }
        public String lat { get; set; }


        public ProductJsonDataObject()
        {


        }
        public ProductJsonDataObject(String Title_, String Desc_, String Hashtags_, String Price_, String SellerId_, String Timestamp_, String Type_, String Lon_,string Lat_)
        {
            title = Title_;
            desc = Desc_;
            tags = Hashtags_;
            price = Price_;
            lon = Lon_;
            lat= Lat_;
            userID = SellerId_;
            Timestamp = Timestamp_;
            Type = Type_;
            
        }
    }
}
