﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baija_dev
{
    
    public class userSettings
    {
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        public string username { get; set; }
        public string apikey { get; set; }
        public string loginstatus { get; set; }
        public string userid { get; set; }
        public string loginfetchdone { get; set; }


        public userSettings()
        {
            if(settings.Contains("username"))
            username = settings["username"].ToString();
            else Debug.WriteLine("username not found");


            if (settings.Contains("userid"))
            userid = settings["userid"].ToString();
            else Debug.WriteLine("userid not found");


            if (settings.Contains("apikey"))
            apikey = settings["apikey"].ToString();
            else Debug.WriteLine("apikey not found");


            if (settings.Contains("loginstatus"))
            loginstatus = settings["loginstatus"].ToString();
            else Debug.WriteLine("loginstatus not found");


            if (settings.Contains("loginfetchdone"))
                loginfetchdone = settings["loginfetchdone"].ToString();
            else Debug.WriteLine("loginfetchdone not found");



            settings.Save();
        }
        public void SaveUserSetting(string key,string value)
        {

            if (settings.Contains(key))
            {
                settings[key] = value;
            }

            else
            {
                settings.Add(key, value);
            }

            settings.Save();

        }
        public bool checkIfKeyExists(string key){

            if (settings.Contains(key))
                return true;

            else
                return false;
        }
            
    
       
      
    }
}
