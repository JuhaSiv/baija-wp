﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baija_dev
{
    class Images
    {

        public string mongoId_ { get; set; }
        public string userId_ { get; set; }
        public string saleId_ { get; set; }
        public string mData_ { get; set; }
        
   
    
    
        public Images()
        {
            mongoId_ = "";
            userId_ = "";
            saleId_ = "";
            mData_ = "";
       
        }

        public Images(string mongoId,string userId,string saleId,string mData)
        {
            mongoId_ = mongoId;
            userId_ = userId;
            saleId_ = saleId;
            mData_ = mData;
        }
  }
}
