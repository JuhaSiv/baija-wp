﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using System.Windows.Media.Effects;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;
using Microsoft.Phone.Shell;
using Windows.Data.Json;
using Windows.Web.Http;
using System.IO.IsolatedStorage;
using System.Collections.ObjectModel;

namespace Baija_dev
{
    public partial class BrowseProducts : PhoneApplicationPage
    {
        bool noSearchResults = true;
        int currentItem = 0;
        bool isGrey = true;
        double ScreenWidth;
        double ScreenHeight;
        bool searchShown = false;
        TextBlock radiusValue= new TextBlock();
        TextBlock priceValue = new TextBlock();
        List<Product> ProductList = new List<Product>();
        List<Images> ImageList = new List<Images>();
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        public BrowseProducts()
        {
           InitializeComponent();

            //Gets Screenwidth and screenheight
           ScreenWidth = Application.Current.Host.Content.ActualWidth;
           ScreenHeight = Application.Current.Host.Content.ActualHeight;

           //LayoutRoot = Main grid from xaml, set layout to screenwidth and height.
           LayoutRoot.Height = ScreenHeight;
           LayoutRoot.Width = ScreenWidth;

            //collapse searchpage
          // HideableSearchPage.Visibility = System.Windows.Visibility.Collapsed;

          // ContentPanel.Height = ScreenHeight - NavBar.ActualHeight*2;
           this.DataContext = this;
           PageNumber = 1;

           Button butt = new Button();
            

            //Set pivot size
           BrowseSinglePivot.Width = (ScreenWidth - 25.0 );
           
            //Get radius / price label textblocks.
           radiusValue = RadiusValue;
           priceValue = PriceValue;
           CreatePivotItem("","Title","500","25");
           //CreatePivotItem("", "Title2", "300", "15");

        }
       protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {


            if (settings.Contains("SearchParameters") && settings.Contains("radius") && settings.Contains("price"))
            {
                SearchField.Text = settings["SearchParameters"].ToString();
                Radius.Value =Convert.ToDouble((settings["radius"]));
                PriceRange.Value = Convert.ToDouble((settings["price"]));
            }
            DatabaseHelperClass db = new DatabaseHelperClass();
            if (ProductList.Count  == 0)
            {
                Debug.WriteLine("ProductList.Count  == 0");
                ObservableCollection<ProductsTable> searchHistory = db.getPreviousSearchResults();
                if (searchHistory.Count > 0) 
                {
                     //DatabaseHelperClass db = new DatabaseHelperClass();
                    Debug.WriteLine("searchHistory.Count > 0");
                    foreach (var product in searchHistory)
                    {
                        Debug.WriteLine("Each product in query prints this line");
                        ProductList.Add(new Product(product.title_, product.desc_, product.tags_, product.price_, product.mongoId_, product.userId_, product.username_, product.timestamp_, product.noteType_, product.distance_));
                     
                        ImagesTable query = db.getFirstProductImage(product.mongoId_);
                        if(query != null)
                        ImageList.Add(new Images(query.saleId_, query.userId_, query.saleId_, query.mData_));
                    }
                   
                    //Boolean means if the search needs to be dragged up after insertting list to pivot. On navigation this is not required
                    ListToPivot(false);
                }
            }

            else
            { //Boolean means if the search needs to be dragged up after insertting list to pivot. On navigation this is not required
                ListToPivot(false);
            }

        }


        public static readonly DependencyProperty PageNumberProperty =
           DependencyProperty.Register("PageNumber", typeof(int), typeof(BrowseProducts), new PropertyMetadata(0));
        /// <summary>
        /// Gets or sets the current page number
        /// </summary>
        /// <remarks>Setting this property has no effect in the demo app</remarks>
        public int PageNumber
        {
            get { return (int)GetValue(PageNumberProperty); }
            set { SetValue(PageNumberProperty, value); }
        }

        private void onSearchPressed(object sender, EventArgs e)
        {

        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/MainScreen.xaml", UriKind.Relative));
            base.OnBackKeyPress(e);

        } 

    /*    private void ContentControl_SwipeLeft(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Swipeleft");
            ++this.PageNumber;
            UpdateContentControl();
        }

        private void ContentControl_SwipeRight(object sender, EventArgs e)
        {
            --this.PageNumber;
            UpdateContentControl();
        }

        private void UpdateContentControl()
        {
            uxContentPanel.CanSwipeRight = (this.PageNumber > 0);
            uxContentPanel.CanSwipeLeft = (this.PageNumber < 2);
            if (PageNumber == 0)
            {
                NavigationService.Navigate(new Uri("/MainScreen.xaml", UriKind.Relative));
            }
            else if (PageNumber == 1)
            {
                NavigationService.Navigate(new Uri("/BrowseProducts.xaml", UriKind.Relative));
            }
            else if (PageNumber == 2)
            {
                NavigationService.Navigate(new Uri("/FavoritesPage.xaml", UriKind.Relative));
            }

        }
        */

        private void SearchButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            double CalculatedSize;
            if (!searchShown)
            {
                CalculatedSize = ScreenHeight - (NavBar.ActualHeight*2 );
                searchShown = true;

                HideableSearchPage.Visibility = System.Windows.Visibility.Visible;
                AnimateResize_Height(0, CalculatedSize, HideableSearchPage);

            }

            else
            {
                CalculatedSize = ScreenHeight - (NavBar.ActualHeight*2);
                AnimateResize_Height(CalculatedSize, 0, HideableSearchPage);
                
                //HideableSearchPage.Visibility = System.Windows.Visibility.Collapsed;
                searchShown = false;

            }

        }

        private void AnimateResize_Height(double startHeight, double endHeight, UIElement targetElement)
        {
            HideableSearchPage.Opacity = 1;
            DoubleAnimation resize = new DoubleAnimation();
            resize.From =startHeight;
            resize.To = endHeight;
            resize.Duration = new Duration(TimeSpan.FromSeconds(0.4));

            var storyBoard = new Storyboard();
            Storyboard.SetTarget(resize, targetElement);
            Storyboard.SetTargetProperty(resize, new PropertyPath("Height"));
            storyBoard.Children.Add(resize);
            storyBoard.Begin();
        }


        private void RadiusListener(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Math.Floor(e.NewValue) < 180)
            {
                radiusValue.Text = Math.Floor(e.NewValue).ToString() + "km";
                Debug.WriteLine(Math.Floor(e.NewValue));
            }
            else{
                radiusValue.Text = ">180km";
            }
        
        }

        private void PriceListener(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

            if (Math.Floor(e.NewValue) < 1501)
            {
                priceValue.Text = Math.Floor(e.NewValue).ToString() + "€";
                Debug.WriteLine(Math.Floor(e.NewValue));
            }

            else
            {
                priceValue.Text = ">1500€";
                Debug.WriteLine(Math.Floor(e.NewValue));
            }
        }
        private void saveSettings(string key,string value)
        {

            if(settings.Contains(key))
            {
                settings[key] = value;
            }
            else
            {
                settings.Add(key,value);
               
            }
            settings.Save();

        }


        private void doSearchButton_click(object sender, RoutedEventArgs e)
        {
      
            string searchString = SearchField.Text;
            string[] SearchParameters = searchString.Split('#');
            string QueryString = "?query=";

            foreach(string queryWord in SearchParameters)
            {
                QueryString += queryWord.Trim() + ",";
            }

            QueryString = QueryString.Remove(QueryString.Length-1);


            saveSettings("SearchParameters",searchString);

            saveSettings("price", PriceRange.Value.ToString());

            saveSettings("radius", Radius.Value.ToString());

            


            Debug.WriteLine("Price:" + priceValue.Text + "\n" + "Radius: " + radiusValue.Text + "\n" + QueryString);
           doWebRequest(QueryString);
        }
        private async Task doWebRequest(string queryString)
        {
         getLocation Location = new getLocation();

         string latitude = Location.Latitude.ToString().Replace(",", ".");
         string longitude = Location.Longitude.ToString().Replace(",", ".");
         string url = "http://www.baija.fi/product/search/" + queryString + "&lat=" + latitude + "&lon=" + longitude + "&price=" + priceValue.Text + "&radius=" + radiusValue.Text;
            Debug.WriteLine(url);
            // HTTP web request


            JsonWebClient client = new JsonWebClient();
            var resp = await client.DoRequestAsync(url);
            string result = resp.ReadToEnd();
            if (!string.IsNullOrEmpty(result))
            {
                DatabaseHelperClass db = new DatabaseHelperClass();
                db.DeleteTemps();
               await handleJSONResponse(result);
            }
            //Debug.WriteLine(result);

        }
        async Task handleJSONResponse(string response)
        {

            ProductList.Clear();
            JObject obj = JObject.Parse(response);
            JArray products = (JArray)obj["products"];
            foreach(var product in products)
            {
                Product newProduct = new Product();

                newProduct.Id_ = product["id"].ToString();
                newProduct.Title_ = product["title"].ToString();
                newProduct.Distance_ = product["distance"].ToString();
                newProduct.Desc_ = product["description"].ToString();
                newProduct.SellerId_ = product["userId"].ToString();
                newProduct.SellerName_ = product["userName"].ToString();
                newProduct.Price_ = product["price"].ToString();

               string tags = product["tags"].ToString();

               newProduct.Hashtags_ = tags;
                
                newProduct.Timestamp_ = product["timestamp"].ToString();
                ProductList.Add(newProduct);

            await getPictures(product["id"].ToString());
            }
            if(listMode.IsChecked == true){

                PhoneApplicationService.Current.State["product"] = ProductList;
                NavigationService.Navigate(new Uri("/Views/BrowseList.xaml", UriKind.Relative));

            }
            else
            {
                //Boolean means if the search needs to be dragged up after insertting list to pivot.
                ListToPivot(true);
            }
         
        }
        private async Task getPictures(string mongoId)
        {

            string url = "http://www.baija.fi/product/pics/?id=" + mongoId;
            Debug.WriteLine(url);
            // HTTP web request


            JsonWebClient client = new JsonWebClient();
            var resp = await client.DoRequestAsync(url);
            string result = resp.ReadToEnd();
            if (!string.IsNullOrEmpty(result))
            {
              await handlePictureData(result);
            }
            //Debug.WriteLine(result);


        }
        private async Task handlePictureData(string response)
        {
//            Debug.WriteLine(response);
          //  JsonValue jsonvalue = JsonValue.Parse(response);
         //   Debug.WriteLine(jsonvalue.GetObject().GetNamedString("pictures").ToString());
            if (!response.Equals("{\"pictures\": \"No pictures found\"}"))
            {
                Debug.WriteLine("iffailut gg");
                JObject obj = JObject.Parse(response);
                JArray pictures = (JArray)obj["pictures"];
                Debug.WriteLine("badabum");
                foreach (var picture in pictures)
                {
                    Debug.WriteLine("ping");
                    ImagesTable newImage = new ImagesTable();

                    Debug.WriteLine("Each picture in list prints this ");
                 
                   
                    newImage.mongoId_ = picture["id"].ToString();
                    newImage.userId_ = picture["userID"].ToString();
                    newImage.saleId_ = picture["saleID"].ToString();
                    newImage.mData_ = picture["mData"].ToString();
                    
                    ImageList.Add(new Images(newImage.saleId_, newImage.userId_, newImage.saleId_, newImage.mData_));
                    Debug.WriteLine("kuva");
                    DatabaseHelperClass db = new DatabaseHelperClass();
                    db.InsertPicture(newImage);
                         
                }
            }
            
            
        }
        private async Task ListToPivot(bool dragSearchUp)
        {
            Debug.WriteLine("List to pivot called");
            DatabaseHelperClass db_Helper = new DatabaseHelperClass();
            BrowseSinglePivot.Items.Clear();
           // db_Helper.DeleteTemps();
          
            foreach(var product in ProductList)
            {
                string imgData = "";
                Debug.WriteLine("product: ");
                foreach (Images image in ImageList)
                {
                  
                    if (image.saleId_.Equals(product.Id_))
                    {
                        Debug.WriteLine(image.saleId_ + " - " + product.Id_);
                        imgData = image.mData_;
                
                        break;
                    }
                }
                await CreatePivotItem(imgData, product.Title_, product.Price_, product.Distance_);

                db_Helper.Insert(new ProductsTable(product.Id_, product.SellerId_, product.SellerName_, product.Title_, product.Desc_, product.Price_, product.Timestamp_, "", product.Distance_, "temp","sell"),"temp");

            }
            if (dragSearchUp)
            {
                SearchButton_Tap(null, null);
            }
        
        }


        BitmapImage convertFromBase4(string s)
        {
            Debug.WriteLine( "converfrom"+ s);
            byte[] fileBytes = Convert.FromBase64String(s);
           
            using (MemoryStream ms = new MemoryStream(fileBytes, 0, fileBytes.Length))
            {
                ms.Write(fileBytes, 0, fileBytes.Length);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(ms);
                return bitmapImage;
            }
        }

        private void FadeOut(int From, int To, bool isGrey)
        {
            DoubleAnimation resize = new DoubleAnimation();
            resize.From = From;
            resize.To = To;
            resize.Duration = new Duration(TimeSpan.FromSeconds(0.5));

            var storyBoard = new Storyboard();
            Storyboard.SetTarget(resize, FavouriteStar);
            Storyboard.SetTargetProperty(resize, new PropertyPath("Opacity"));
            storyBoard.Children.Add(resize);
            storyBoard.Begin();
            storyBoard.Completed += (s, a) =>
            {
                if (isGrey)
                {
                    FavouriteStar.Source = new BitmapImage(new Uri(@"/Assets/star.png", UriKind.RelativeOrAbsolute));
                    fadeIn(0, 1, isGrey);
                }
                else
                {
                    FavouriteStar.Source = new BitmapImage(new Uri(@"/Assets/grey_star.png", UriKind.RelativeOrAbsolute));
                    fadeIn(0, 1, isGrey);
                }

            };

        }
        private void fadeIn(int From, int To, bool isGrey)
        {
            DoubleAnimation resize = new DoubleAnimation();
            resize.From = From;
            resize.To = To;
            resize.Duration = new Duration(TimeSpan.FromSeconds(0.5));

            var storyBoard = new Storyboard();
            Storyboard.SetTarget(resize, FavouriteStar);
            Storyboard.SetTargetProperty(resize, new PropertyPath("Opacity"));
            storyBoard.Children.Add(resize);
            storyBoard.Begin();
        }

        private async Task CreatePivotItem(string img64base, string title, string price, string distance)
        {
            Debug.WriteLine("Create pivot item called");
            int rowCount = 3;
            int columnCount = 2;
            TextBlock Title = new TextBlock();
            TextBlock Price = new TextBlock();
            TextBlock Distance = new TextBlock();

            Title.Text = title;
            Price.Text = price + " €";
            Distance.Text = distance + " km";

            Price.Padding = new Thickness(15,0,0,0);
            Distance.Padding = new Thickness(0, 0, 15, 0);

            Title.FontSize = 45;
            Title.FontWeight = FontWeights.Bold; 
            Price.FontSize = 35;
            Distance.FontSize = 35;

            
            PivotItem pivotItem = new PivotItem();
            Grid containerGrid = new Grid();

            //pivotItem.Content = containerGrid;
            /*Create row definitions for each row in grid
             * 
             * Sets grid row height to '*' 
             * 
             * */


            for (int i = 0; i < rowCount;i++ )
            {
                containerGrid.RowDefinitions.Add(new RowDefinition());
            }

            bool first = true;
            foreach ( var rowDefinition in containerGrid.RowDefinitions)
            {

                if(first)
                {
                    rowDefinition.Height = new GridLength(1, GridUnitType.Star);
                    first = false;
                }
               
                else
                {
                    rowDefinition.Height = new GridLength(1, GridUnitType.Auto);
                }
            }




            /*
             * Column definitions for each column in grid
             * 
             * sets grid column width to '*'
             * */
            for (int i = 0; i < columnCount; i++)
            {
                containerGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }
   
            foreach (var columnDefinition in containerGrid.ColumnDefinitions)
            {
                columnDefinition.Width = new GridLength(1,GridUnitType.Star);
            }


            Image templateImage = new Image();
            Rectangle roundedImage = new Rectangle();
            Rectangle imageDropShadow = new Rectangle();



            roundedImage.RadiusX = 35;
            roundedImage.RadiusY = 35;
            roundedImage.Width = ScreenWidth * 4/5;
            roundedImage.Height = ScreenWidth * 4 / 5;



            imageDropShadow.Width = ScreenWidth * 4 / 5; 
            imageDropShadow.Height = ScreenWidth * 4 / 5; 
            imageDropShadow.RadiusX = 35;
            imageDropShadow.RadiusY = 35;
            imageDropShadow.Opacity = 0.3;
            imageDropShadow.RenderTransformOrigin = new Point(0,0);
            imageDropShadow.StrokeThickness = 16;
            imageDropShadow.StrokeDashCap = PenLineCap.Round;
            imageDropShadow.StrokeEndLineCap = PenLineCap.Round;
            imageDropShadow.StrokeLineJoin = PenLineJoin.Round;
            imageDropShadow.StrokeStartLineCap = PenLineCap.Round;
            imageDropShadow.Stroke = new SolidColorBrush(Color.FromArgb(255,0,0,0));
            CompositeTransform scale = new CompositeTransform();
            scale.ScaleX = 1.02;
            scale.ScaleY = 1.02;
            imageDropShadow.RenderTransform = scale;

            ImageBrush imageBrush = new ImageBrush();

           

         
            if (!img64base.Equals(""))
            {
                Debug.WriteLine("templateen base64");
            //    templateImage.Source = convertFromBase4(img64base);
                imageBrush.ImageSource = convertFromBase4(img64base);
            }
            else
            {
                templateImage.Source = new BitmapImage(new Uri(@"/Assets/canon.jpg", UriKind.RelativeOrAbsolute));
                imageBrush.ImageSource = new BitmapImage(new Uri(@"/Assets/canon.jpg", UriKind.RelativeOrAbsolute));
            }
            roundedImage.Fill = imageBrush;
         //   templateImage
            containerGrid.CacheMode = new BitmapCache();
           

            containerGrid.Children.Add(imageDropShadow);
            Grid.SetColumn(imageDropShadow, 0);
            Grid.SetColumnSpan(imageDropShadow, 2);
            Grid.SetRow(imageDropShadow, 0);

            containerGrid.Children.Add(roundedImage);
            Grid.SetColumn(roundedImage, 0);
            Grid.SetColumnSpan(roundedImage, 2);
            Grid.SetRow(roundedImage, 0);



            containerGrid.Children.Add(Title);
            Title.TextAlignment = TextAlignment.Center;
            Grid.SetRow(Title, 1);
            Grid.SetColumn(Title, 0);
            Grid.SetColumnSpan(Title,2);

            Rectangle infoDropShadow = new Rectangle ();

            infoDropShadow.Width = ScreenWidth * 4 / 5;
            infoDropShadow.Height = double.NaN;
            infoDropShadow.RadiusX = 10;
            infoDropShadow.RadiusY = 10;
            infoDropShadow.Opacity = 0.3;
            infoDropShadow.RenderTransformOrigin = new Point(0, 0);
            infoDropShadow.StrokeThickness = 6;
            infoDropShadow.StrokeDashCap = PenLineCap.Round;
            infoDropShadow.StrokeEndLineCap = PenLineCap.Round;
            infoDropShadow.StrokeLineJoin = PenLineJoin.Round;
            infoDropShadow.StrokeStartLineCap = PenLineCap.Round;
            infoDropShadow.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            CompositeTransform infoScale = new CompositeTransform();
            infoScale.ScaleX = 1.01;
            infoScale.ScaleY = 1.02;
            infoDropShadow.RenderTransform = infoScale;


            containerGrid.Children.Add(infoDropShadow);
            Grid.SetRow(infoDropShadow, 2);
            Grid.SetColumn(infoDropShadow, 0);
            Grid.SetColumnSpan(infoDropShadow, 2);
            

            Rectangle infoRect = new Rectangle();

            infoRect.Width = ScreenWidth * 4 / 5;
            infoRect.Height = double.NaN;
            infoRect.RadiusX = 10;
            infoRect.RadiusY = 10;
          
           

            infoRect.Fill = new SolidColorBrush(Color.FromArgb(75, 255, 255, 255));




            containerGrid.Children.Add(infoRect);
            Grid.SetRow(infoRect,2);
            Grid.SetColumn(infoRect,0);
            Grid.SetColumnSpan(infoRect,2);

            containerGrid.Children.Add(Price);
            Price.TextAlignment = TextAlignment.Left;
            Grid.SetRow(Price, 2);
            Grid.SetColumn(Price, 0);
           // Grid.SetColumnSpan(Price, 2);

            containerGrid.Children.Add(Distance);
            Distance.TextAlignment = TextAlignment.Right;
            Grid.SetRow(Distance, 2);
            Grid.SetColumn(Distance, 1);


                pivotItem.Content = containerGrid;

            BrowseSinglePivot.Items.Add(pivotItem);
           // BrowseSinglePivot.

        }

        private void Remove_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MessageBoxResult mb = MessageBox.Show("You wont be able to see this item after removing.\nAre you sure you want to remove it from the list?", "Confirm", MessageBoxButton.OKCancel);

            if (mb == MessageBoxResult.OK)
            {

              //TODO: RemoveFromListing(currentItem);
             
            }


        }

        private void openProduct_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {

            PhoneApplicationService.Current.State["product"] = ProductList.ElementAt(BrowseSinglePivot.SelectedIndex);
            DatabaseHelperClass db = new DatabaseHelperClass();
            if (db.checkIfOwn(ProductList.ElementAt(BrowseSinglePivot.SelectedIndex).Id_))
            {
                NavigationService.Navigate(new Uri("/Views/OwnProductPage.xaml", UriKind.Relative));
            }
            else
            {
                NavigationService.Navigate(new Uri("/Views/ProductPage.xaml", UriKind.Relative));
            }
            
       
        }

        private void FavouriteStar_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {


             DatabaseHelperClass db = new DatabaseHelperClass();

             Product p = ProductList.ElementAt(currentItem);
            if (db.checkIfOwn(p.Id_))
            {
                MessageBoxResult mb = MessageBox.Show("You cant favorite your own product", "Confirm", MessageBoxButton.OK);

                if (mb == MessageBoxResult.OK)
                {

                }
            }
            else
            {
               if( db.checkIfFav(p.Id_)){

                   removeFavorite();
                   FadeOut(1, 0, false);

                }
               else
               {
                   doFavorite();
                   FadeOut(1, 0, true);
                   
               }
              
            }
            //Swap the boolean from true -> false or false -> true
        
            isGrey = !isGrey;
        
        }

        private async Task removeFavorite()
        {
                    DatabaseHelperClass db = new DatabaseHelperClass();
                 string id = ProductList.ElementAt(currentItem).Id_;
                 string url = " http://www.baija.fi/product/deletefavorite/";
                 Uri urlString = new Uri(url);

                 Dictionary<string, string> pairs = new Dictionary<string, string>();
                 pairs.Add("apikey", settings["apikey"].ToString());
                 pairs.Add("userID", settings["userid"].ToString());
                 pairs.Add("delete", id);




                 HttpFormUrlEncodedContent formContent =
                  new HttpFormUrlEncodedContent(pairs);

                 HttpClient client = new HttpClient();
                 HttpResponseMessage response = await client.PostAsync(urlString, formContent);

                 Debug.WriteLine(response.Content);

                 JsonValue jsonvalue = JsonValue.Parse(response.Content.ToString());
                 if (jsonvalue.GetObject().GetNamedString("status").Equals("success"))
                 {
                     db.DeleteFromFav(id);
                 }

        }
        private async Task doFavorite(){



            DatabaseHelperClass db = new DatabaseHelperClass();

            Product p = ProductList.ElementAt(currentItem);

                 string id = ProductList.ElementAt(currentItem).Id_;
                 string url = " http://www.baija.fi/product/favorites/";
                 Uri urlString = new Uri(url);

                 Dictionary<string, string> pairs = new Dictionary<string, string>();
                 pairs.Add("apikey", settings["apikey"].ToString());
                 pairs.Add("userID", settings["userid"].ToString());
                 pairs.Add("favorites", id);




                 HttpFormUrlEncodedContent formContent =
                  new HttpFormUrlEncodedContent(pairs);

                 HttpClient client = new HttpClient();
                 HttpResponseMessage response = await client.PostAsync(urlString, formContent);

                 Debug.WriteLine(response.Content);

                 JsonValue jsonvalue = JsonValue.Parse(response.Content.ToString());
                 if (jsonvalue.GetObject().GetNamedString("status").Equals("success"))
                 {
                     //ProductsTable(string mongoId,string userId,string username,string title,string desc,string price,string timestamp,string tags, string distance, string type,string noteType)
                     db.addFav(new ProductsTable(p.Id_, p.SellerId_, p.SellerName_, p.Title_, p.Desc_, p.Price_, p.Timestamp_, p.Hashtags_, p.Distance_, "fav", ""));
                 }

             
         }

        private void PivotSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DatabaseHelperClass db = new DatabaseHelperClass();
      
            if(ProductList.Count > 0)
            {
                 currentItem = BrowseSinglePivot.SelectedIndex;
                 if (db.checkIfFav(ProductList.ElementAt(currentItem).Id_))
                 {
                            
                        //Change to gold star if favorited
                        FadeOut(1, 0, true);
                      //  isGrey = false;
                        //Swap the boolean from true -> false or false -> true
               
                }
                else
                {
                   //Change color to black if not favorited.
                    FadeOut(1, 0, false);
                 //   isGrey = true;
                    //Swap the boolean from true -> false or false -> true
                
                }
            }
           
            

         
        }

        private void buyNotes_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            sellMode.IsChecked = false;
        }

        private void sellMode_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            buyMode.IsChecked = false;
        }

        private void buyMode_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            sellMode.IsChecked = false;
        }

        private void slideMode_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            listMode.IsChecked = false;
        }

        private void listMode_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            slideMode.IsChecked = false;
        }
    }
}