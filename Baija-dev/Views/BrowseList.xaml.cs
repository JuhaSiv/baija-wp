﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using System.Windows.Media.Animation;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.IO;

namespace Baija_dev
{
    public partial class BrowseList : PhoneApplicationPage
    {
        int currentItem = 0;
        bool isFavorited = false;
        double ScreenWidth;
        double ScreenHeight;
        bool searchShown = false;
        TextBlock radiusValue = new TextBlock();
        TextBlock priceValue = new TextBlock();
        List<Product> ProductList = new List<Product>();
        List<Images> ImageList = new List<Images>();
        int PageNumber;


        public BrowseList()
        {
            InitializeComponent();
            SearchResultList.SelectedItem = null;
          //  ProductList = new List<Product>();


            ScreenWidth = Application.Current.Host.Content.ActualWidth;
            ScreenHeight = Application.Current.Host.Content.ActualHeight;

            //LayoutRoot = Main grid from xaml, set layout to screenwidth and height.
            LayoutRoot.Height = ScreenHeight;
            LayoutRoot.Width = ScreenWidth;

            //collapse searchpage
            // HideableSearchPage.Visibility = System.Windows.Visibility.Collapsed;

            // ContentPanel.Height = ScreenHeight - NavBar.ActualHeight*2;
            this.DataContext = this;
           // PageNumber = 1;

            //Button butt = new Button();
     

            //Set pivot size
        

            //Get radius / price label textblocks.
            radiusValue = RadiusValue;
            priceValue = PriceValue;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DatabaseHelperClass db = new DatabaseHelperClass();
            var product = PhoneApplicationService.Current.State["product"] as List<Product>;
            SearchResultList.SelectedItem = null; ;
            ProductList = product;
            for(int i =0; i < ProductList.Count;i++)
            {

                var query = db.getFirstProductImage(ProductList.ElementAt(i).Id_);
                if (query != null)
                {

                    ProductList.ElementAt(i).previewImg_ = convertFromBase64(query.mData_);
                }
                else
                {

                    ProductList.ElementAt(i).previewImg_ = new BitmapImage(new Uri(@"/Assets/canon.jpg", UriKind.RelativeOrAbsolute));
                }            
            }
            SearchResultList.ItemsSource = ProductList;
            //base.OnNavigatedTo(e);
        }
        BitmapImage convertFromBase64(string s)
        {
            Debug.WriteLine("converfrom" + s);
            byte[] fileBytes = Convert.FromBase64String(s);

            using (MemoryStream ms = new MemoryStream(fileBytes, 0, fileBytes.Length))
            {
                ms.Write(fileBytes, 0, fileBytes.Length);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(ms);
                return bitmapImage;
            }
        }
        private void searchResultList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Get Currently selected, can be null.
            var selectedItem = (Product)SearchResultList.SelectedItem;

            //Check if anything is selected.
            if (selectedItem == null)
            {

                return;
            }

            //Id of currently selected element
            var id = selectedItem.Id_;

            //Navigate to product page and pass ID of currently selected item to the product page
            //**********************TODO: OwnProductPage.xaml***************************
            NavigationService.Navigate(new Uri("/Views/ProductPage.xaml?id=" + id, UriKind.Relative));

            //Set currently selected item to null 
            //to avoid problems with going back to list and selecting same item (No itemChanged event)
            SearchResultList.SelectedItem = null;

        }

        private void SearchButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            double CalculatedSize;
            if (!searchShown)
            {
                CalculatedSize = ScreenHeight - (NavBar.ActualHeight * 2);
                searchShown = true;

                HideableSearchPage.Visibility = System.Windows.Visibility.Visible;
                AnimateResize_Height(0, CalculatedSize, HideableSearchPage);

            }

            else
            {
                CalculatedSize = ScreenHeight - (NavBar.ActualHeight * 2);
                AnimateResize_Height(CalculatedSize, 0, HideableSearchPage);

                //HideableSearchPage.Visibility = System.Windows.Visibility.Collapsed;
                searchShown = false;

            }

        }

        private void AnimateResize_Height(double startHeight, double endHeight, UIElement targetElement)
        {
            HideableSearchPage.Opacity = 1;
            DoubleAnimation resize = new DoubleAnimation();
            resize.From = startHeight;
            resize.To = endHeight;
            resize.Duration = new Duration(TimeSpan.FromSeconds(0.4));

            var storyBoard = new Storyboard();
            Storyboard.SetTarget(resize, targetElement);
            Storyboard.SetTargetProperty(resize, new PropertyPath("Height"));
            storyBoard.Children.Add(resize);
            storyBoard.Begin();
        }


        private void RadiusListener(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Math.Floor(e.NewValue) < 180)
            {
                radiusValue.Text = Math.Floor(e.NewValue).ToString() + "km";
                Debug.WriteLine(Math.Floor(e.NewValue));
            }
            else
            {
                radiusValue.Text = ">180km";
            }

        }

        private void PriceListener(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

            if (Math.Floor(e.NewValue) < 1501)
            {
                priceValue.Text = Math.Floor(e.NewValue).ToString() + "€";
                Debug.WriteLine(Math.Floor(e.NewValue));
            }

            else
            {
                priceValue.Text = ">1500€";
                Debug.WriteLine(Math.Floor(e.NewValue));
            }
        }

        private void doSearchButton_click(object sender, RoutedEventArgs e)
        {
            string searchString = SearchField.Text;
            string[] SearchParameters = searchString.Split('#');
            string QueryString = "?query=";

            foreach (string queryWord in SearchParameters)
            {
                QueryString += queryWord.Trim() + ",";
            }

            QueryString = QueryString.Remove(QueryString.Length - 1);




            Debug.WriteLine("Price:" + priceValue.Text + "\n" + "Radius: " + radiusValue.Text + "\n" + QueryString);
            doWebRequest(QueryString);
        }
        private async Task doWebRequest(string queryString)
        {
            getLocation Location = new getLocation();


            string url = "http://www.baija.fi/product/search/" + queryString + "&lat=" + Location.Latitude + "&lon=" + Location.Longitude + "&price=" + priceValue.Text + "&radius=" + radiusValue.Text;
            Debug.WriteLine(url);
            // HTTP web request


            JsonWebClient client = new JsonWebClient();
            var resp = await client.DoRequestAsync(url);
            string result = resp.ReadToEnd();
            if (!string.IsNullOrEmpty(result))
            {
                handleJSONResponse(result);
            }
            //Debug.WriteLine(result);

        }
               async Task handleJSONResponse(string response)
        {

            ProductList.Clear();
            JObject obj = JObject.Parse(response);
            JArray products = (JArray)obj["products"];
            foreach(var product in products)
            {
                Product newProduct = new Product();

                newProduct.Id_ = product["id"].ToString();
                newProduct.Title_ = product["title"].ToString();
                newProduct.Distance_ = product["distance"].ToString();
                newProduct.Desc_ = product["description"].ToString();
                newProduct.SellerId_ = product["userId"].ToString();
                newProduct.SellerName_ = product["userName"].ToString();
                newProduct.Price_ = product["price"].ToString();

               string tags = product["tags"].ToString();

               newProduct.Hashtags_ = tags;
                
                newProduct.Timestamp_ = product["timestamp"].ToString();
                ProductList.Add(newProduct);

            await getPictures(product["id"].ToString());
            }
            if(listMode.IsChecked != true){

                PhoneApplicationService.Current.State["product"] = ProductList;
                NavigationService.Navigate(new Uri("/Views/BrowseProducts.xaml", UriKind.Relative));

            }
            else
            {
                //Boolean means if the search needs to be dragged up after insertting list to pivot.
                //ListToPivot(true);
            }
         
        }
        private async Task getPictures(string mongoId)
        {

            string url = "http://www.baija.fi/product/pics/?id=" + mongoId;
            Debug.WriteLine(url);
            // HTTP web request


            JsonWebClient client = new JsonWebClient();
            var resp = await client.DoRequestAsync(url);
            string result = resp.ReadToEnd();
            if (!string.IsNullOrEmpty(result))
            {
              await handlePictureData(result);
            }
            //Debug.WriteLine(result);


        }
        private async Task handlePictureData(string response)
        {
            //            Debug.WriteLine(response);
            //  JsonValue jsonvalue = JsonValue.Parse(response);
            //   Debug.WriteLine(jsonvalue.GetObject().GetNamedString("pictures").ToString());
            if (!response.Equals("{\"pictures\": \"No pictures found\"}"))
            {
                Debug.WriteLine("iffailut gg");
                JObject obj = JObject.Parse(response);
                JArray pictures = (JArray)obj["pictures"];
                Debug.WriteLine("badabum");
                foreach (var picture in pictures)
                {
                    Debug.WriteLine("ping");
                    ImagesTable newImage = new ImagesTable();

                    Debug.WriteLine("Each picture in list prints this ");


                    newImage.mongoId_ = picture["id"].ToString();
                    newImage.userId_ = picture["userID"].ToString();
                    newImage.saleId_ = picture["saleID"].ToString();
                    newImage.mData_ = picture["mData"].ToString();

                    ImageList.Add(new Images(newImage.saleId_, newImage.userId_, newImage.saleId_, newImage.mData_));
                    Debug.WriteLine("kuva");





                    DatabaseHelperClass db = new DatabaseHelperClass();
                    db.InsertPicture(newImage);


                    for (int i = 0; i < ProductList.Count; i++)
                    {
                        if (newImage.saleId_.Equals(ProductList.ElementAt(i).Id_))
                        {


                            var query = db.getFirstProductImage(ProductList.ElementAt(i).Id_);
                            if (query != null)
                            {

                                ProductList.ElementAt(i).previewImg_ = convertFromBase64(query.mData_);
                            }
                            else
                            {

                                ProductList.ElementAt(i).previewImg_ = new BitmapImage(new Uri(@"/Assets/canon.jpg", UriKind.RelativeOrAbsolute));
                            }

                        }

                    }
                }


            }
        }
        private void ListToList()
        {

            SearchResultList.ItemsSource = new List<Product>();

         
            SearchResultList.ItemsSource = ProductList;
           
            SearchButton_Tap(null, null);
        }
        private void buyNotes_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            sellMode.IsChecked = false;
        }

        private void sellMode_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            buyMode.IsChecked = false;
        }

        private void buyMode_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            sellMode.IsChecked = false;
        }

        private void slideMode_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            listMode.IsChecked = false;
        }

        private void listMode_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            slideMode.IsChecked = false;
        }

        private void openSelectedProduct_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var selectedItem = (Product)SearchResultList.SelectedItem;
            PhoneApplicationService.Current.State["product"] = selectedItem;
            NavigationService.Navigate(new Uri("/Views/ProductPage.xaml", UriKind.Relative));

            //Set currently selected item to null 
            //to avoid problems with going back to list and selecting same item (No itemChanged event)
            SearchResultList.SelectedItem = null;
        }
    }
}