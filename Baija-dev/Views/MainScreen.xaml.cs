﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using Windows.Web.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Baija_dev.dbClasses;
using System.Windows.Threading;

namespace Baija_dev
{

    public partial class MainScreen : PhoneApplicationPage
    {
        DispatcherTimer timer = new DispatcherTimer();
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        public MainScreen()
        {
           InitializeComponent();
           this.DataContext = this;
       //    PageNumber = 0;
           LayoutRoot.Height = Application.Current.Host.Content.ActualHeight;
           LayoutRoot.Width = Application.Current.Host.Content.ActualWidth;
           welcomeUser.Text = "Welcome " + settings["username"].ToString();
           Debug.WriteLine(settings["loginstatus"]);
           
            /* Leave out the view modxel for the demo and bind to the form itself */
        }

  /*      public static readonly DependencyProperty PageNumberProperty =
           DependencyProperty.Register("PageNumber", typeof(int), typeof(MainScreen), new PropertyMetadata(0));
        /// <summary>
        /// Gets or sets the current page number
        /// </summary>
        /// <remarks>Setting this property has no effect in the demo app</remarks>
        public int PageNumber
        {
            get { return (int)GetValue(PageNumberProperty); }
            set { SetValue(PageNumberProperty, value); }
        }*/
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {

            if (settings["loginfetchdone"].ToString().Equals("false"))
           {
               DatabaseHelperClass db = new DatabaseHelperClass();
               db.ClearDataBase();

               fetchMessagesFromMongo();
               fetchOwnFromMongo();
               fetchFavFromMongo();

               settings["loginfetchdone"] = "true";
               settings.Save();

               timer.Tick += timer_Tick;
               timer.Interval = new TimeSpan(00, 0, 5);
               bool enabled = timer.IsEnabled;
               timer.Start();

           }
        }

        private void timer_Tick(object sender, object e)
        {
            fetchMessagesFromMongo();
        }
   
        private string formatTimeToSeconds(string timestamp)
        {
            DateTime dt = DateTime.Parse(timestamp);
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan span = (dt - epoch);

            //Add 1 seconds because API checks if greater or equal
            double unixTime = span.TotalSeconds+1;
           

            return unixTime.ToString();
        }

  



        private async Task fetchMessagesFromMongo()
        {

            string url = "http://www.baija.fi/product/allmessages/";
            Uri urlString = new Uri(url);

            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("apikey", settings["apikey"].ToString());
            pairs.Add("id", settings["userid"].ToString());
            DatabaseHelperClass db = new DatabaseHelperClass();
            string latest = db.getLatestMessageTimestamp();
            if (!latest.Equals(""))
            {


                string seconds = formatTimeToSeconds(latest);
                // Debug.WriteLine(seconds);
                pairs.Add("seconds", seconds);
            }



            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);

            Debug.WriteLine(response.Content);

            handleResponseForChatmessages(response.Content.ToString());
        }

        private async Task handleResponseForChatmessages(string response)
        {
  
            JObject obj = JObject.Parse(response);
            

            foreach (var o in obj["chatmessages"])
            {
                string chatPartnerName ="";
                //Debug.WriteLine("O TU SRINK"+o.ToString());
                JProperty property = o.Value<JProperty>();
                var name = property.Name;
                Debug.WriteLine(name.ToString());
                Debug.WriteLine("Ou mai gaad" + o.First["type"].ToString());

                //Parsing for someone else's products
                if (o.First["type"].ToString().Equals("normal"))
                {
                    var messages = o.First["messages"];
                    chatPartnerName = o.First["username"].ToString();
                    foreach (var message in messages)
                    {
                        string mesId = message["id"].ToString();
                        string mesSender = message["sender"].ToString();
                        string mesReceiver = message["receiver"].ToString();
                        string mesContent = message["content"].ToString();
                        string mesType = message["type"].ToString();
                        string mesTimestamp = message["timestamp"].ToString();

                 
                       
                            ChatTable newMessage = new ChatTable(mesSender, mesReceiver, mesContent, mesTimestamp, chatPartnerName, mesId, name.ToString());
                            DatabaseHelperClass db = new DatabaseHelperClass();
                            db.InsertChatMessage(newMessage);
                    }
                }
                    //Parsing for own products
                else
                {
                    Dictionary<string, string> chatPartners = new Dictionary<string, string>();
                    foreach (var u in o.First["username"])
                    {
                        JProperty userProperty = u.Value<JProperty>();
                        chatPartners.Add(userProperty.Name, userProperty.Value.ToString());
                    }


                    var messages = o.First["messages"];
                    foreach (var chats in messages)
                    {
                        Debug.WriteLine("chat foreach");
                        JProperty chatProperty = chats.Value<JProperty>();
                        chatPartnerName = chatPartners[chatProperty.Name];
                        foreach (var chat in chats)
                        {

                            foreach (var message in chat)
                            {
                                Debug.WriteLine("message foreach");
                                string mesId = message["id"].ToString();
                                string mesSender = message["sender"].ToString();
                                string mesReceiver = message["receiver"].ToString();
                                string mesContent = message["content"].ToString();
                                string mesType = message["type"].ToString();
                                string mesTimestamp = message["timestamp"].ToString();

                                ChatTable newMessage = new ChatTable(mesSender, mesReceiver, mesContent, mesTimestamp, chatPartnerName, mesId, name.ToString());
                                DatabaseHelperClass db = new DatabaseHelperClass();
                                db.InsertChatMessage(newMessage);
                                //end else
                            }//end foreach
                        }//end foreach
                    }

                }

            }


        }


        private async Task fetchOwnFromMongo()
        {

            string url = "http://www.baija.fi/product/own/?id=" + settings["userid"].ToString();
            Uri urlString = new Uri(url);

            Dictionary<string, string> pairs = new Dictionary<string, string>();
 





            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);

            Debug.WriteLine(response.Content);

            handleResponseForProducts(response.Content.ToString(),"own");
        }
        private async Task fetchFavFromMongo()
        {

            string url = "http://www.baija.fi/product/favorites/?get=" + settings["userid"].ToString();
            Uri urlString = new Uri(url);
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("apikey", settings["apikey"].ToString());





            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);

            Debug.WriteLine(response.Content);

            handleResponseForProducts(response.Content.ToString(), "fav");
        }

        private async Task handleResponseForProducts(string response,string type)
        {
            JObject obj = JObject.Parse(response);
            JArray products = (JArray)obj["products"];
            foreach (var product in products)
            {
                Product newProduct = new Product();

                newProduct.Id_ = product["id"].ToString();
                newProduct.Title_ = product["title"].ToString();
                newProduct.Distance_ = product["distance"].ToString();
                newProduct.Desc_ = product["description"].ToString();
                newProduct.SellerId_ = product["userId"].ToString();
                newProduct.SellerName_ = product["userName"].ToString();
                newProduct.Price_ = product["price"].ToString();

                string tags = product["tags"].ToString();

                newProduct.Hashtags_ = tags;

                newProduct.Timestamp_ = product["timestamp"].ToString();

                DatabaseHelperClass db = new DatabaseHelperClass();
                db.Insert(new ProductsTable(newProduct.Id_, newProduct.SellerId_, newProduct.SellerName_, newProduct.Title_, newProduct.Desc_, newProduct.Price_, newProduct.Timestamp_, newProduct.Hashtags_, newProduct.Distance_, type, "sell"),type);
                fetchImagesForProduct(newProduct.Id_);
            
            }

        }

        private async Task fetchImagesForProduct(string mongoId)
        {
            string url = "http://www.baija.fi/product/pics/?id=" + mongoId;
            Debug.WriteLine(url);
            // HTTP web request


            JsonWebClient client = new JsonWebClient();
            var resp = await client.DoRequestAsync(url);
            string result = resp.ReadToEnd();
            if (!string.IsNullOrEmpty(result))
            {
                await handlePictureData(result);
            }

        }
        private async Task handlePictureData(string response)
        {
            if (!response.Equals("{\"pictures\": \"No pictures found\"}"))
            {
                Debug.WriteLine("iffailut gg");
                JObject obj = JObject.Parse(response);
                JArray pictures = (JArray)obj["pictures"];
                Debug.WriteLine("badabum");
                foreach (var picture in pictures)
                {
                    Debug.WriteLine("ping");
                    ImagesTable newImage = new ImagesTable();

                    Debug.WriteLine("Each picture in list prints this ");


                    newImage.mongoId_ = picture["id"].ToString();
                    newImage.userId_ = picture["userID"].ToString();
                    newImage.saleId_ = picture["saleID"].ToString();
                    newImage.mData_ = picture["mData"].ToString();

                    Debug.WriteLine("kuva");
                    DatabaseHelperClass db = new DatabaseHelperClass();
                    db.InsertPicture(newImage);

                }
            }
            



        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult mb = MessageBox.Show("You want exit the app", "Alert", MessageBoxButton.OKCancel);

            if (mb == MessageBoxResult.OK)
            {


                // e.Cancel = true;
                Application.Current.Terminate();
            }
            else
            {
                e.Cancel = true;
            }
            base.OnBackKeyPress(e);

        }  

        

   /*     private void ContentControl_SwipeLeft(object sender, EventArgs e)
        {
            Debug.WriteLine("Swipeleft");
            ++this.PageNumber;
            UpdateContentControl();
        }

        private void ContentControl_SwipeRight(object sender, EventArgs e)
        {
            --this.PageNumber;
            UpdateContentControl();
        }

        private void UpdateContentControl()
        {
            uxContentPanel.CanSwipeRight = (this.PageNumber > 0);
            uxContentPanel.CanSwipeLeft = (this.PageNumber < 2);
            if (PageNumber == 0)
            {
                NavigationService.Navigate(new Uri("/MainScreen.xaml", UriKind.Relative));
            }
            else if (PageNumber == 1)
            {
                NavigationService.Navigate(new Uri("/BrowseProducts.xaml", UriKind.Relative));
            }
            else if (PageNumber == 2)
            {
                NavigationService.Navigate(new Uri("/FavoritesPage.xaml", UriKind.Relative));
            }

        }*/

        private void LogOut_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (settings.Contains("loginstatus"))
            {
                settings.Remove("loginstatus");
            }
            if (settings.Contains("UserID"))
            {
                settings.Remove("UserID");
            }
            if (settings.Contains("APIKey"))
            {
                settings.Remove("APIKey");
            }
            if (settings.Contains("startupfetchdone"))
            {
                settings.Remove("startupfetchdone");
            }
            settings.Save();

            DatabaseHelperClass db = new DatabaseHelperClass();
            db.ClearDataBase();
            //TODO: muut logoutit
            NavigationService.Navigate(new Uri("/Views/MainPage.xaml", UriKind.Relative));
            
        }
    }
}