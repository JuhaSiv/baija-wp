﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Web.Http;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
using Windows.Security.Cryptography;
using System.Diagnostics;
using System.IO.IsolatedStorage;

namespace Baija_dev
{
    public partial class RegisterPage : PhoneApplicationPage
    {
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        public RegisterPage()
        {
            InitializeComponent();
        }

        private void passwordConfirm_inputStart(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            pwHintRe.Opacity = 0;
        }

        private void password_inputStart(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            pwHint.Opacity = 0;
        }

        private void password_lostFocus(object sender, RoutedEventArgs e)
        {
            if (password.Password.ToString().Length == 0)
            {
                pwHint.Opacity = 1;
            }
        }

        private void passwordConfirm_lostFocus(object sender, RoutedEventArgs e)
        {
            if (passwordConfirm.Password.ToString().Length == 0)
            {
                pwHintRe.Opacity = 1;
            }
        }

        private void passwordConfirm_passwordChanged(object sender, RoutedEventArgs e)
        {
            pwHintRe.Opacity = 0;
        }

        private void password_passwordChanged(object sender, RoutedEventArgs e)
        {
            pwHint.Opacity = 0;
        }

        private void doRegister_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
    
            if (password.Password.ToString().Length > 5){

                if (password.Password.ToString().Equals(passwordConfirm.Password.ToString()))
                {
                    doRegister();
                }
                else
                {
                    ShellToast toast = new ShellToast();
                    toast.Title = "Password";
                    toast.Content = "Passwords dont match.";
                    toast.Show();
                }
           
            }
            else{
                ShellToast toast = new ShellToast();
                toast.Title = "Password";
                toast.Content = "Password too short.";
                toast.Show();
            }

            
        }




        public string CalculateMD5Hash(string str)
        {
            var alg = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);
            IBuffer buff = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8);
            var hashed = alg.HashData(buff);
            var res = CryptographicBuffer.EncodeToHexString(hashed);
            return res;
        }


        private async Task doRegister()
        {
            string url = "http://www.baija.fi/register/";
            Uri urlString = new Uri(url);
            ListPickerItem selectedItem = (ListPickerItem)gender.SelectedItem;
            getLocation currentLocation = new getLocation();
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("email", registerEmail.Text);
            pairs.Add("first", first.Text);
            pairs.Add("last", last.Text);
            pairs.Add("age", age.Text);
            pairs.Add("gender", selectedItem.Content.ToString());
            pairs.Add("pass", CalculateMD5Hash(password.Password.ToString()));
            Debug.WriteLine("" + registerEmail.Text + " " + first.Text+ " " +last.Text + " " +age.Text+ " " +gender.SelectedItem.ToString()+ " " +CalculateMD5Hash(password.Password.ToString()));


            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);

            Debug.WriteLine("RESPONSE: " + response.Content);
            if (response.Content == null)
            {
                ShellToast toast = new ShellToast();
                toast.Title = "Connection error";
                toast.Content = "Unable to connect server, try again later.";
                toast.Show();
            }
            else
            {
                JsonValue jsonvalue = JsonValue.Parse(response.Content.ToString());
                if (jsonvalue.GetObject().GetNamedString("status").Equals("success"))
                {
                    await doLogin(registerEmail.Text, CalculateMD5Hash(password.Password.ToString()));
                }
            }
        
        }

        private async Task doLogin(string email, string password)
        {
            Debug.WriteLine("doLogin call");
            string url = "http://www.baija.fi/login/";
            Uri urlString = new Uri(url);
            Debug.WriteLine("email" + email + " - pass: " + password);
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("email", email);
            pairs.Add("pass", password);



            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);

            Debug.WriteLine(response.Content);
            JsonValue jsonvalue = JsonValue.Parse(response.Content.ToString());
            Debug.WriteLine(jsonvalue);
            if (jsonvalue.GetObject().GetNamedString("status").Equals("success"))
            {
                Debug.WriteLine("If");
                string status = "LoggedIn";
                string userID = jsonvalue.GetObject().GetNamedString("id");
                string ApiKEY = jsonvalue.GetObject().GetNamedString("apikey");
                Debug.WriteLine(status + " " + userID + " " + ApiKEY);
                setLoginSettings(status, userID, ApiKEY);


            }



        }
        private void setLoginSettings(string status,string userID,string ApiKEY)
        {
            // LoggedIn = logged in status
            if (!settings.Contains("loginstatus"))
            {
                settings.Add("loginstatus", status);
            }
            else
            {
                settings["loginstatus"] = status;
            }
            if (!settings.Contains("UserID"))
            {
                settings.Add("UserID", userID);
            }
            else
            {
                settings["userid"] = userID;
            }
            if (!settings.Contains("APIKey"))
            {
                settings.Add("APIKey", ApiKEY);
            }
            else
            {
                settings["apikey"] = ApiKEY;

            }


            if (!settings.Contains("startupfetchdone"))
            {
                settings.Add("startupfetchdone", "false");
            }
            else
            {
                settings["startupfetchdone"] = "false";
            }

            settings.Save();
            NavigationService.Navigate(new Uri("/Views/MainScreen.xaml", UriKind.Relative));
        }
    }
}