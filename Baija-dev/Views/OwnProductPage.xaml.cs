﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using System.IO;
using Windows.Web.Http;
using System.IO.IsolatedStorage;
using Windows.Data.Json;
using System.Threading.Tasks;

namespace Baija_dev
{
    public partial class OwnProductPage : PhoneApplicationPage
    {

        Product ownProduct = new Product();
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        public OwnProductPage()
        {
            InitializeComponent();
       

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            ownProduct = (Product)PhoneApplicationService.Current.State["product"];
            titleBlock.Text = ownProduct.Title_;
            priceBlock.Text = ownProduct.Price_;
            descBlock.Text = ownProduct.Desc_;
            tagBlock.Text = ownProduct.Hashtags_;
            fetchImagesForProduct(ownProduct.Id_);

        }

        private void removeOwnSale_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
             MessageBoxResult mb = MessageBox.Show("Are you sure, that you want to remove this product? This cannot be undone. You will have to remake the note.", "Confirm", MessageBoxButton.OKCancel);

             if (mb == MessageBoxResult.OK)
             {
                 doRemoveOwnProduct();
             }
        }

        private async Task doRemoveOwnProduct()
        {
            string url = "http://www.baija.fi/product/delete/?id=" + ownProduct.Id_;
            Uri urlString = new Uri(url);
            getLocation currentLocation = new getLocation();
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("apikey", settings["apikey"].ToString());
            //pairs.Add("productID", ownProduct.Id_);


            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response;
            
              response = await client.PostAsync(urlString, formContent);
         
         

            Debug.WriteLine("RESPONSE: " + response);
            if (response.Content == null)
            {
                ShellToast toast = new ShellToast();
                toast.Title = "Connection error";
                toast.Content = "Unable to connect server, try again later.";
                toast.Show();
            }
            else
            {
                JsonValue jsonvalue = JsonValue.Parse(response.Content.ToString());
                if (jsonvalue.GetObject().GetNamedString("status").Equals("success"))
                {
                    DatabaseHelperClass db = new DatabaseHelperClass();
                   
                    
                        db.DeleteFromOwn(ownProduct.Id_);
                   
                        Debug.WriteLine("Delete successful");
                        NavigationService.GoBack();
                    

                    
                }
            }
        }

        private void backButton_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void fetchImagesForProduct(string id)
        {
            int count = 1;
            Image imageToChange = new Image();
            DatabaseHelperClass db = new DatabaseHelperClass();
            var query = db.getAllPicturesOfProduct(id);

            foreach (var image in query)
            {
                Debug.WriteLine("Image in query");
                if (count == 1)
                {
                    imageToChange = productPic1;
                }
                else if (count == 2)
                {
                    imageToChange = productPic2;
                }
                else if (count == 3)
                {
                    imageToChange = productPic3;
                }
                else if (count == 4)
                {
                    imageToChange = productPic4;
                }
                count++;

                imageToChange.Source = convertFromBase64(image.mData_);
            }
        }
        BitmapImage convertFromBase64(string s)
        {
            Debug.WriteLine("converfrom" + s);
            byte[] fileBytes = Convert.FromBase64String(s);

            using (MemoryStream ms = new MemoryStream(fileBytes, 0, fileBytes.Length))
            {
                ms.Write(fileBytes, 0, fileBytes.Length);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(ms);
                return bitmapImage;
            }
        }
    }
}