﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using Microsoft.Devices;
using System.IO;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework.Media;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;
using System.Device.Location;
using System.Threading.Tasks;
using Windows.Data.Json;
using Newtonsoft.Json;
using Windows.Web.Http;
using Newtonsoft.Json.Linq;
using System.Text;


namespace Baija_dev
{
    public partial class addSalePage : PhoneApplicationPage
    {
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        double screenWidth;
        double screenHeight;
        JsonObject data = new JsonObject();
        Image getImage1 = new Image();
        Image targetImage = new Image();
        BitmapImage defaultImage;
    

        public addSalePage()
        {

            InitializeComponent();
            defaultImage = new BitmapImage(new Uri(@"/Assets/baija_logo.png", UriKind.RelativeOrAbsolute));
            screenWidth = Application.Current.Host.Content.ActualWidth;
            screenHeight = Application.Current.Host.Content.ActualHeight;

            mediaCaptureOverlay.Width = screenWidth * 4 / 5;
            mediaCaptureOverlay.Height = screenHeight * 1 / 2;
            getImage1 = image1;
        }

        private void backButton_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void takePicture(object sender, System.Windows.Input.GestureEventArgs e)
        {

            

            mediaCaptureOverlay.Visibility = Visibility.Visible;
          //  Image selectedPic = (Image)(sender as Image).DataContext;
           // string name = selectedPic.Name;
            targetImage = (Image)sender;
            Debug.WriteLine(((Image)sender).Name);
        }

        private void openCamera_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            CameraCaptureTask cameraCaptureTask = new CameraCaptureTask();
            cameraCaptureTask.Completed += new EventHandler<PhotoResult>(cameraCaptureTask_Completed);
            cameraCaptureTask.Show();
            mediaCaptureOverlay.Visibility = Visibility.Collapsed;
        }

        private void openLibrary_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhotoChooserTask photoChooserTask = new PhotoChooserTask();
            photoChooserTask.Completed += new EventHandler<PhotoResult>(photoChooserTask_Completed);
            photoChooserTask.Show();
            mediaCaptureOverlay.Visibility = Visibility.Collapsed;
        }

        private void removePicture_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
           
            targetImage.Source = new BitmapImage(new Uri("Assets/baija_logo.png", UriKind.RelativeOrAbsolute));
            mediaCaptureOverlay.Visibility = Visibility.Collapsed;
        }

        private void cancel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        
            mediaCaptureOverlay.Visibility = Visibility.Collapsed;
        }

        void cameraCaptureTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
              //  MessageBox.Show(e.ChosenPhoto.Length.ToString());

                //Code to display the photo on the page in an image control named myImage.
                System.Windows.Media.Imaging.BitmapImage bmp = new System.Windows.Media.Imaging.BitmapImage();
                bmp.SetSource(e.ChosenPhoto);
                targetImage.Source = bmp;
                targetImage = null;

            }
        }
        void photoChooserTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                Debug.WriteLine(e.ChosenPhoto.ToString());

                //Code to display the photo on the page in an image control named myImage.
                System.Windows.Media.Imaging.BitmapImage bmp = new System.Windows.Media.Imaging.BitmapImage();
                bmp.SetSource(e.ChosenPhoto);
                targetImage.Source = bmp;
               // convertToBase64((BitmapImage)targetImage.Source);
                targetImage = null;
            }
        }
        
        //converToBase64(bmp); tai converToBase64((BitmapImage)image.Source);
        string convertToBase64(BitmapImage target)
        {
            byte[] byteArray = null;
            using (MemoryStream ms = new MemoryStream())
            {
                if (target == null)
                {

                }
                else
                {
                    WriteableBitmap wbitmp = new WriteableBitmap(target);
                    wbitmp.SaveJpeg(ms, 350, 350, 0, 35);
                    byteArray = ms.ToArray();
                }
            }
            string str = Convert.ToBase64String(byteArray);

            Debug.WriteLine(str.Length);
            return str;
        }
        /*Convertteri muistiiin */
        BitmapImage convertFromBase4(string s)
        {
            byte[] fileBytes = Convert.FromBase64String(s);

            using (MemoryStream ms = new MemoryStream(fileBytes, 0, fileBytes.Length))
            {
                ms.Write(fileBytes, 0, fileBytes.Length);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(ms);
                return bitmapImage;
            }
        }

        private void postSale_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Send_button.Content = "Sending";
            Send_button.UpdateLayout();
            getLocation currentLocation = new getLocation();
            DateTime currentDate = DateTime.Now;
            string defaultPath = convertToBase64((BitmapImage)image0.Source);
            string[] postData = new string[] {};
            if (convertToBase64((BitmapImage)image1.Source).Equals(defaultPath) &&
                convertToBase64((BitmapImage)image2.Source).Equals(defaultPath) && 
                convertToBase64((BitmapImage)image3.Source).Equals(defaultPath) &&
                convertToBase64((BitmapImage)image4.Source).Equals(defaultPath))
            {
               MessageBoxResult mb = MessageBox.Show("Are you sure you want to add post without pictures?", "Confirm", MessageBoxButton.OKCancel);

                if (mb == MessageBoxResult.OK)
                {
                  

                  //  String[] hashtagTable = productHashtag.Text.Trim().Split('#');



                    ProductJsonDataObject productToSend = new ProductJsonDataObject(productTitle.Text, productDesc.Text, productHashtag.Text, productPrice.Text, settings["userid"].ToString(), "sell", currentDate.ToString(), currentLocation.Longitude, currentLocation.Latitude);

                

                    string json = JsonConvert.SerializeObject(productToSend);
                    Debug.WriteLine("Title: " + productTitle.Text);
                    Debug.WriteLine("Desc: " + productDesc.Text);
                    Debug.WriteLine("Price: " + productPrice.Text);
                    Debug.WriteLine("Hashtags: " + productHashtag.Text);
                    Debug.WriteLine("NoteType:" + getNoteType());
                   // Debug.WriteLine("Lat: " + latitude);
                    Debug.WriteLine("Lon: " + currentLocation.Longitude);

                    Debug.WriteLine("JSONDATA:" + json);
              
                  doPostSale(json);
                }
                else
                {
                    LoadingOverlay.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                
                ProductJsonDataObject productToSend = new ProductJsonDataObject(productTitle.Text, productDesc.Text, productHashtag.Text, productPrice.Text, settings["userid"].ToString(), "sell", currentDate.ToString(), currentLocation.Longitude, currentLocation.Latitude);
                string json = JsonConvert.SerializeObject(productToSend);
                doPostSale(json);

            }

         
        }

     
      
        private async Task doPostSale(string json)
        {
            string url = "http://www.baija.fi/product/add/";
            Uri urlString = new Uri(url);

            getLocation currentLocation = new getLocation();

            string latitude = currentLocation.Latitude.ToString().Replace(",", ".");
            string longitude = currentLocation.Longitude.ToString().Replace(",", ".");
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("userID", settings["userid"].ToString());
            pairs.Add("apikey", settings["apikey"].ToString());
            pairs.Add("title", productTitle.Text);
            pairs.Add("desc", productDesc.Text);
            pairs.Add("price", productPrice.Text);
            pairs.Add("tags", productHashtag.Text);
            pairs.Add("lon", longitude);
            pairs.Add("lat", latitude);

            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);

            Debug.WriteLine("RESPONSE: " + response.Content);
            if (response.Content == null)
            {
                ShellToast toast = new ShellToast();
                toast.Title = "Connection error";
                toast.Content = "Unable to connect server, try again later.";
                toast.Show(); 
            }
            else
            {
                JsonValue jsonvalue = JsonValue.Parse(response.Content.ToString());
                if (jsonvalue.GetObject().GetNamedString("status").Equals("success"))
                {
                    string id = jsonvalue.GetObject().GetNamedString("id");

                    DatabaseHelperClass db_Helper = new DatabaseHelperClass();
                    db_Helper.Insert(new ProductsTable(id, settings["userid"].ToString(), "My Own", productTitle.Text, productDesc.Text, productPrice.Text, "timestamp", productHashtag.Text, "0", "own",getNoteType()),"own");


                    Debug.WriteLine("parsittu id:" + id);
                    string defaultPath = convertToBase64((BitmapImage)image0.Source);
                    if (!convertToBase64((BitmapImage)image1.Source).Equals(defaultPath))
                    {
                       Debug.WriteLine( convertToBase64((BitmapImage)image1.Source).Length + "");
                       await doPostPics(id, convertToBase64((BitmapImage)image1.Source));
                    }
                    if(!convertToBase64((BitmapImage)image2.Source).Equals(defaultPath)){
                        await doPostPics(id, convertToBase64((BitmapImage)image2.Source));
                    }
                    if(!convertToBase64((BitmapImage)image3.Source).Equals(defaultPath)){
                        await doPostPics(id, convertToBase64((BitmapImage)image3.Source));
                    }
                    if (!convertToBase64((BitmapImage)image4.Source).Equals(defaultPath))
                    {
                        await doPostPics(id, convertToBase64((BitmapImage)image4.Source));
                    }


                    Send_button.Content = "Post It";
                    NavigationService.Navigate(new Uri("/Views/FavoritesPage.xaml", UriKind.Relative));
                }
                else
                {


                    string reason = jsonvalue.GetObject().GetNamedString("reason");

                    ShellToast toast = new ShellToast();
                    toast.Title = "fail";
                    toast.Content = reason;
                    toast.Show();
                }

            }
         
           
        }

        private async Task doPostPics(string saleID,string pictureString)
        {

            string url = "http://www.baija.fi/product/addpic/";
            Uri urlString = new Uri(url);

            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("apikey", settings["apikey"].ToString());
            pairs.Add("picture", pictureString);
            pairs.Add("saleID", saleID);
     

            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);

            JsonValue jsonvalue = JsonValue.Parse(response.Content.ToString());
            if (jsonvalue.GetObject().GetNamedString("status").Equals("success"))
            {
                string mongoId = jsonvalue.GetObject().GetNamedString("id");
                DatabaseHelperClass db = new DatabaseHelperClass();
                ImagesTable newImage = new ImagesTable(mongoId, settings["userid"].ToString(), saleID, pictureString, "own");
                db.InsertPicture(newImage);

            }
            
          


        }
        static void GetLocationProperty()
        {
            GeoCoordinateWatcher watcher = new GeoCoordinateWatcher();

            // Do not suppress prompt, and wait 1000 milliseconds to start.
            watcher.TryStart(false, TimeSpan.FromMilliseconds(1000));

            GeoCoordinate coord = watcher.Position.Location;

            if (coord.IsUnknown != true)
            {
                Debug.WriteLine("Lat: {0}, Long: {1}",
                    coord.Latitude,
                    coord.Longitude);
            }
            else
            {
                Debug.WriteLine("Unknown latitude and longitude.");
            }
        }

        private void sellNote_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            buyButton.IsChecked = false;
          

        }

        private void buyNote_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

      
            sellButton.IsChecked = false;
        }

        private string getNoteType()
        {
            string noteType ="";
            if (buyButton.IsChecked == true)
            {
                noteType = "Buy";
            }
            else noteType = "Sell";

            return noteType;
        }
        
    }
}