﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using System.IO.IsolatedStorage;
using Windows.Web.Http;
using Windows.Data.Json;
using Baija_dev.dbClasses;
using Newtonsoft.Json.Linq;

namespace Baija_dev
{
    public partial class chatPage : PhoneApplicationPage
    {
        DispatcherTimer timer = new DispatcherTimer();
        DateTime currentDate = DateTime.Now;
        string productId = "";
        string latestMessageDateSring = "";
        string receiverId = "";
        string receiverName = "";
        string timestamp = "";
        string message = "";
        string chatPartnerName="";
        Product product = new Product();
        ChatTable chatInfo = new ChatTable();
        List<ChatTable> chatMessageList = new List<ChatTable>();
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        public chatPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            product = PhoneApplicationService.Current.State["product"] as Product;
        
            chatInfo = PhoneApplicationService.Current.State["chat"] as ChatTable;
            //TODO: CHECK IF NULL
            productId = product.Id_;
            if (chatInfo == null)
            {
                receiverId = product.SellerId_;

            }
            else
            { 
                if (settings["userid"].Equals(chatInfo.senderId_))
                {
                    receiverId = chatInfo.receiverId_;
                }
                else 
                {
                    receiverId = chatInfo.senderId_;
                }
            }
            ProductName.Text = product.Title_;
          

            
          
           timer.Tick += timer_Tick;
           timer.Interval = new TimeSpan(00, 0, 5);
           bool enabled = timer.IsEnabled;
           timer.Start();

           DatabaseHelperClass db = new DatabaseHelperClass();
          var query = db.getAllMessagesForThisChat(productId, settings["userid"].ToString(),receiverId);
          foreach (ChatTable message in query)
          {
              Debug.WriteLine("message in query");
              //public ChatTable(string senderId, string receiverId, string message, string timestamp, string chatPartnerName, string mongoId, string productId)
              ChatTable messageFromSqlite = new ChatTable(message.senderId_,message.receiverId_,message.message_,message.timestamp_,message.chatPartnerName_,message.mongoId_,message.productId_);
              chatMessageList.Add(messageFromSqlite);
              //      setMessageToScreen(settings["userid"].ToString(), messageField.Text.ToString(), timestamp, settings["username"].ToString());
              setMessageToScreen(message.senderId_, message.message_, message.timestamp_, "");
          }

          if (settings["userid"].ToString().Equals(product.SellerId_) && chatInfo != null)
          {
             chatName.Text = "Chat - " + chatInfo.chatPartnerName_;
          }
          else
          {
              chatName.Text = "Chat - " + product.SellerName_;
              Debug.WriteLine("");
          }

          PhoneApplicationService.Current.State["chat"] = null;
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            timer.Stop();
        }

        private void timer_Tick(object sender, object e)
        {
            updateMessageLog();
        }

        private void sendMessage_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            doSendMessage();
        }

        private async Task doSendMessage(){
            
            string url = "http://www.baija.fi/product/newmessage/";
            Uri urlString = new Uri(url);
          //  Debug.WriteLine("email" + email + " - pass: " + password);
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("apikey", settings["apikey"].ToString());
            pairs.Add("productID", productId);
            pairs.Add("senderID", settings["userid"].ToString());
            pairs.Add("receiverID", receiverId);
            pairs.Add("content", messageField.Text.ToString());
            pairs.Add("type", "text");

          //  pairs.Add("seconds", password);



            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);
            Debug.WriteLine(response.Content);
            JsonValue jsonvalue = JsonValue.Parse(response.Content.ToString());
       
            if (jsonvalue.GetObject().GetNamedString("status").Equals("success"))
            {
               // Debug.WriteLine("Message sent!");
                string timestamp = jsonvalue.GetObject().GetNamedString("timestamp");
                string messageId = jsonvalue.GetObject().GetNamedString("id");
                setMessageToScreen(settings["userid"].ToString(), messageField.Text.ToString(), timestamp, settings["username"].ToString());


                ChatTable newMessage = new ChatTable(settings["userid"].ToString(), receiverId, messageField.Text.ToString(), timestamp, chatPartnerName, messageId, productId);
                chatMessageList.Add(newMessage);
                
               

                DatabaseHelperClass db = new DatabaseHelperClass();
                db.InsertChatMessage(newMessage);
                messageField.Text = "";
            }

        }

        private async Task updateMessageLog()
        {
          Debug.WriteLine("Method Call");

            string url = "http://www.baija.fi/product/allmessages/";
            Uri urlString = new Uri(url);
          //  Debug.WriteLine("email" + email + " - pass: " + password);
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("apikey", settings["apikey"].ToString());
            pairs.Add("id", settings["userid"].ToString());
            if (chatMessageList.Count > 0)
            {
            //  var latestmessage =  chatMessageList.ElementAt(chatMessageList.Count - 1);
              DatabaseHelperClass db = new DatabaseHelperClass();
             string latest = db.getLatestMessageTimestamp();
             string seconds = formatTimeToSeconds(latest);
            // Debug.WriteLine(seconds);
             pairs.Add("seconds", seconds);
            }
       



            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);

            Debug.WriteLine(response.Content);

            handleResponse(response.Content.ToString());
        }
        private string formatTimeToSeconds(string timestamp)
        {
            DateTime dt = DateTime.Parse(timestamp);
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan span = (dt - epoch);

            //Add 1 seconds because API checks if greater or equal
            double unixTime = span.TotalSeconds+1;
           

            return unixTime.ToString();
        }

        private async Task handleResponse(string response)
        {
           // JsonValue jsonvalue = JsonValue.Parse(response);

            //dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(jsonvalue.ToString());

          //  Debug.WriteLine(obj);
            //JArray a = JArray.Parse(jsonvalue.GetObject().ToString());

            JObject obj = JObject.Parse(response);


            foreach (var o in obj["chatmessages"])
            {

                //Debug.WriteLine("O TU SRINK"+o.ToString());
                JProperty property = o.Value<JProperty>();
                var name = property.Name;
                Debug.WriteLine(name.ToString());
                Debug.WriteLine("Ou mai gaad" + o.First["type"].ToString());

                //Parsing for someone else's products
                if (o.First["type"].ToString().Equals("normal"))
                {
                       var messages = o.First["messages"];
                        chatPartnerName = o.First["username"].ToString();
                        foreach(var message in messages)
                        {
                            string mesId = message["id"].ToString();
                            string mesSender = message["sender"].ToString();
                            string mesReceiver = message["receiver"].ToString();
                            string mesContent = message["content"].ToString();
                            string mesType = message["type"].ToString();
                            string mesTimestamp = message["timestamp"].ToString();

                            if (checkIfInList(mesId))
                            {

                            }
                            else
                            {
                                ChatTable newMessage = new ChatTable(mesSender, mesReceiver, mesContent, mesTimestamp,chatPartnerName, mesId, name.ToString());
                                if (name.ToString().Equals(productId))
                                {

                                    chatMessageList.Add(newMessage);
                                    setMessageToScreen(mesSender, mesContent, mesTimestamp,"message sender name, irrelevant atm");
                                }
                                DatabaseHelperClass db = new DatabaseHelperClass();
                                db.InsertChatMessage(newMessage); 
                               
                              
                            }
                          //  Debug.WriteLine(message["content"] + " lolkekbur");

                        }
                }

                    //Parsing for own products
                else{
                    

                    Dictionary<string,string> chatPartners = new Dictionary<string,string>();
                    foreach (var u in o.First["username"])
                    {
                        JProperty userProperty = u.Value<JProperty>();
                        chatPartners.Add(userProperty.Name, userProperty.Value.ToString());
                    }


                    var messages = o.First["messages"];
                    foreach (var chats in messages)
                    {
                        Debug.WriteLine("chat foreach");
                        JProperty chatProperty = chats.Value<JProperty>();
                        chatPartnerName = chatPartners[chatProperty.Name];
                        foreach(var chat in chats)
                        {

                            foreach (var message in chat)
                            {
                                Debug.WriteLine("message foreach");
                                string mesId = message["id"].ToString();
                                string mesSender = message["sender"].ToString();
                                string mesReceiver = message["receiver"].ToString();
                                string mesContent = message["content"].ToString();
                                string mesType = message["type"].ToString();
                                string mesTimestamp = message["timestamp"].ToString();

                                if (checkIfInList(mesId))
                                {

                                }
                                else
                                {
                                    ChatTable newMessage = new ChatTable(mesSender, mesReceiver, mesContent, mesTimestamp, chatPartnerName, mesId, name.ToString());
                                    if (name.ToString().Equals(productId))
                                    {

                                        chatMessageList.Add(newMessage);
                                        setMessageToScreen(mesSender, mesContent, mesTimestamp, "message sender name, irrelevant atm");
                                    }
                                    DatabaseHelperClass db = new DatabaseHelperClass();
                                    db.InsertChatMessage(newMessage);


                                }
                               
                            }
                        }
                      

                    }

                }
             

   
            }


        }

        private  bool checkIfInList(string messageId)
        {
            bool idFound = false;
            foreach (ChatTable message in chatMessageList)
            {
                if (message.mongoId_.Equals(messageId))
               {
                    idFound = true;
               }
            }

            return idFound;
        }

        private void setMessageToScreen(string senderUserID,string Message,string timestamp,string username){

            
            Grid newMessageGrid = new Grid();

            newMessageGrid.RowDefinitions.Add(new RowDefinition());
            newMessageGrid.RowDefinitions.Add(new RowDefinition());
            newMessageGrid.ColumnDefinitions.Add(new ColumnDefinition());

            foreach (var columnDefinition in newMessageGrid.ColumnDefinitions)
            {
                columnDefinition.Width = new GridLength(1, GridUnitType.Star);
            }

            TextBlock newMessage = new TextBlock();
            newMessage.Text = Message;

            TextBlock messageTimestamp = new TextBlock();
            messageTimestamp.Text = timestamp;


            if (settings["userid"].ToString().Equals(senderUserID))
            {
              //  newMessageGrid.Children.Add(Message, 0);
                newMessage.TextAlignment = TextAlignment.Right;
                messageTimestamp.TextAlignment = TextAlignment.Right;

                newMessageGrid.Children.Add(newMessage);
                Grid.SetColumn(newMessage, 0);
                Grid.SetRow(newMessage, 0);

                newMessageGrid.Children.Add(messageTimestamp);
                Grid.SetColumn(messageTimestamp, 0);
                Grid.SetRow(messageTimestamp, 1);

                chatContent.Children.Add(newMessageGrid);
            }
            else{

                newMessageGrid.Children.Add(newMessage);
                Grid.SetColumn(newMessage, 0);
                Grid.SetRow(newMessage, 0);

                newMessageGrid.Children.Add(messageTimestamp);
                Grid.SetColumn(messageTimestamp, 0);
                Grid.SetRow(messageTimestamp, 1);

                chatContent.Children.Add(newMessageGrid);
               
            }
            chatMessageViewer.UpdateLayout();
            chatMessageViewer.ScrollToVerticalOffset(chatMessageViewer.ScrollableHeight);
        }

    
    }
}
