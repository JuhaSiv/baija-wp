﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Baija_dev.Resources;
using Windows.Storage;
using System.IO.IsolatedStorage;
using System.Diagnostics;
using Sqlite;
using Baija_dev.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Media.Animation;
using Windows.Security.Cryptography.Core;
using Windows.Web.Http;
using System.Text;
using Windows.Security.Cryptography;
using Windows.Storage.Streams;
using Windows.Data.Json;

namespace Baija_dev
{
    public partial class MainPage : PhoneApplicationPage
    {
        bool loginFieldsVisible = false;
        userSettings usersettings = new userSettings();
        ObservableCollection<ProductsTable> db_productList = new ObservableCollection<ProductsTable>();
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        // Constructor
        public MainPage()
        {
            InitializeComponent();


         /* 
            DatabaseHelperClass db_Helper = new DatabaseHelperClass();

            List<ImagesTable> PictureList = new List<ImagesTable>();
              var query = db_Helper.getAllPictures();
              Debug.WriteLine(query.Count);
            foreach (var pic in query)
            {
                Debug.WriteLine(" " + pic.mData_);
            }

            */ 
            getAllProducts dbProducts = new getAllProducts();
            db_productList = dbProducts.getAllProductsHelper();
            Debug.WriteLine(db_productList.Count);
            foreach(var product in db_productList)
            {
                Debug.WriteLine(product.mongoId_ + " " + product.type_);
            }

            //LogIn_Tap(null,null);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            
            if (usersettings.checkIfKeyExists("loginstatus")) { 
                if (usersettings.loginstatus.Equals("loggedin"))
                {
                    usersettings.SaveUserSetting("loginfetchdone", "false");
                    NavigationService.Navigate(new Uri("/Views/MainScreen.xaml", UriKind.RelativeOrAbsolute));

                }
                else
                {
                    //NavigationService.Navigate(new Uri("/Views/MainPage.xaml", UriKind.Relative));
                }
            }
        }
       private void OnSessionStateChanged(object sender, Facebook.Client.Controls.SessionStateChangedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/MainScreen.xaml", UriKind.Relative));
            this.ContentPanel.Visibility = (e.SessionState == Facebook.Client.Controls.FacebookSessionState.Opened) ?
                                        Visibility.Visible : Visibility.Collapsed;
        }
        
        private void setLoginSettings(string status,string userID,string ApiKEY,string userName)
        {
            // LoggedIn = logged in status

            usersettings.SaveUserSetting("loginstatus", status);

            usersettings.SaveUserSetting("loginfetchdone", "false");

            usersettings.SaveUserSetting("username", userName);

            usersettings.SaveUserSetting("userid", userID);

            usersettings.SaveUserSetting("apikey", ApiKEY);

            NavigationService.Navigate(new Uri("/Views/MainScreen.xaml", UriKind.Relative));
        }
       
        private void baijaLoginBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        
            if (loginFieldsVisible)
            {
           
            
       

                elementFadeOut(loginFields);
                elementFadeOut(registerBtn);

                loginEmail.IsEnabled = false;
                loginPassword.IsEnabled = false;
                loginButton.IsEnabled = false;
                registerBtn.IsEnabled = false;
              
            }
            else
            {
                loginEmail.IsEnabled = true;
                loginPassword.IsEnabled = true;
                loginButton.IsEnabled = true;
                registerBtn.IsEnabled = true;
               

                elementFadeIn(loginFields);
                elementFadeIn(registerBtn);

                
            }

            loginFieldsVisible = !loginFieldsVisible;


        }

        private void elementFadeOut(UIElement target)
        {
            DoubleAnimation fade = new DoubleAnimation();
            fade.From = 1;
            fade.To = 0;
            fade.Duration = new Duration(TimeSpan.FromSeconds(0.7));


            var storyBoard = new Storyboard();
            Storyboard.SetTarget(fade, target);
            Storyboard.SetTargetProperty(fade, new PropertyPath("Opacity"));
            storyBoard.Children.Add(fade);
            storyBoard.Begin();

        }
        private void elementFadeIn(UIElement target)
        {
            DoubleAnimation fade = new DoubleAnimation();
            fade.From = 0;
            fade.To = 1;
            fade.Duration = new Duration(TimeSpan.FromSeconds(0.7));


            var storyBoard = new Storyboard();
            Storyboard.SetTarget(fade, target);
            Storyboard.SetTargetProperty(fade, new PropertyPath("Opacity"));
            storyBoard.Children.Add(fade);
            storyBoard.Begin();

        }
        private void loginBtn_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
           string pw = CalculateMD5Hash(loginPassword.Password.ToString());
           string email = loginEmail.Text;
           doLogin(email, pw);
           Debug.WriteLine("tap");
          
        }

        private async Task doLogin(string email, string password)
        {
            Debug.WriteLine("doLogin call");
            string url = "http://www.baija.fi/login/";
            Uri urlString = new Uri(url);
            Debug.WriteLine("email" + email + " - pass: " + password);
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("email", email);
            pairs.Add("pass", password);
        


            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);

            Debug.WriteLine(response.Content);
           JsonValue jsonvalue = JsonValue.Parse(response.Content.ToString());
           Debug.WriteLine(jsonvalue);
           if (jsonvalue.GetObject().GetNamedString("status").Equals("success"))
           {
               Debug.WriteLine("If");
               string status = "loggedin";
               string userID = jsonvalue.GetObject().GetNamedString("id");
               string ApiKEY= jsonvalue.GetObject().GetNamedString("apikey");
               string username = jsonvalue.GetObject().GetNamedString("username");
               Debug.WriteLine(status + " " + userID + " " + ApiKEY );
               setLoginSettings(status,userID,ApiKEY,username);


           }



        }
        public string CalculateMD5Hash(string str)
        {
            var alg = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);
            IBuffer buff = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8);
            var hashed = alg.HashData(buff);
            var res = CryptographicBuffer.EncodeToHexString(hashed);
            return res;
        }

        private void registerBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            NavigationService.Navigate(new Uri("/Views/RegisterPage.xaml", UriKind.Relative));
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}