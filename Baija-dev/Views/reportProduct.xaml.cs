﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Baija_dev;
using System.Threading.Tasks;
using System.IO.IsolatedStorage;
using Windows.Web.Http;
using System.Diagnostics;
using Windows.Data.Json;

namespace Baija_dev
{

    public partial class reportProduct : PhoneApplicationPage
    {
        bool textFieldVisible = true;
    
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
         Product product = new Product();
         ListPicker lp = new ListPicker();
         PhoneTextBox ptb = new PhoneTextBox();
         ListPickerItem selectedItem;
        

        public reportProduct()
        {
            InitializeComponent();
            lp = ReasonPicker;
            ptb = givenReason;
            lp.SelectedIndex = 0;
            lp.SetValue(Microsoft.Phone.Controls.ListPicker.ItemCountThresholdProperty, 10);
                
           // selectedItem = (ListPickerItem)ReasonPicker.SelectedItem;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            product = PhoneApplicationService.Current.State["product"] as Product;
        }

        private void backButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            NavigationService.GoBack();
        }

        private void reason_selectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (lp.SelectedIndex == -1 || lp.SelectedItem == null)
                return;

           // ListPickerItem selectedItem = (ListPickerItem)ReasonPicker.SelectedItem;
            selectedItem = (sender as ListPicker).SelectedItem as ListPickerItem;

            if (selectedItem != null)
            {

                if (selectedItem.Content.Equals("Other, what?"))
                {
                    textFieldVisible = true;
                }
                else
                {
                    textFieldVisible = false;
                }
            }


            if (!textFieldVisible)
            {
                ptb.Visibility = Visibility.Collapsed;
                ptb.IsEnabled = false;
            }
            else
            {
                ptb.Visibility = Visibility.Visible;
                ptb.IsEnabled = true;
            }

            
           
        }

        private void sendReport_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            doReport();
        }

        private async Task doReport(){

            ListPickerItem selectedItem = (ListPickerItem)ReasonPicker.SelectedItem;
            string reason = selectedItem.Content.ToString();


            string reasonText = givenReason.Text;

            if (textFieldVisible)
            {
                reason = "Other: " + reasonText;
            }

             string url = "http://www.baija.fi/product/addreport/";
            Uri urlString = new Uri(url);
            getLocation currentLocation = new getLocation();
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("apikey",settings["apikey"].ToString());
            pairs.Add("productID", product.Id_);
            pairs.Add("id", settings["userid"].ToString());
            pairs.Add("reason", reason);
 
           


            HttpFormUrlEncodedContent formContent =
             new HttpFormUrlEncodedContent(pairs);

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync(urlString, formContent);

            Debug.WriteLine("RESPONSE: " + response.Content);
            if (response.Content == null)
            {
                ShellToast toast = new ShellToast();
                toast.Title = "Connection error";
                toast.Content = "Unable to connect server, try again later.";
                toast.Show();
            }
            else
            {
                JsonValue jsonvalue = JsonValue.Parse(response.Content.ToString());
                if (jsonvalue.GetObject().GetNamedString("status").Equals("success"))
                {
                   Debug.WriteLine("Report successful");
                }
            }
        
        }
            

        }
 }
