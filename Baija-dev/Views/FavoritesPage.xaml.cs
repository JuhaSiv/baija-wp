﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Windows.Navigation;
using System.Windows.Media.Imaging;
using System.IO;
using Baija_dev.dbClasses;
using Microsoft.Phone.Shell;

namespace Baija_dev
{
    public partial class FavoritesPage : PhoneApplicationPage
    {
        double screenWidth;
        double screenHeight;
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
       
        public FavoritesPage()
        {
            
            InitializeComponent();
            this.DataContext = this;
         //   PageNumber = 2;
            
            screenWidth =  Application.Current.Host.Content.ActualWidth;
            screenHeight = Application.Current.Host.Content.ActualHeight;


            LayoutRoot.Height = screenHeight;
            LayoutRoot.Width = screenWidth;

            FavPivot.Height = screenHeight - (NavBar.Height * 2);
            FavPivot.Width = screenWidth;

            OwnSalesList.Width = screenWidth - 10;

            FavNaviGrid.Width = screenWidth * 9 / 10;


        
           // FavSalesList.ItemsSource = source;
           // ChatList.ItemsSource = source; // TEE CHATTABLE OBJEKTEISTA TÄÄ NI SUN ELÄMÄS HELPOTTUU VITUN URPO ELÄ UNOHA TÄLLÄSTÄ todo: listi chattable objetista

           // FavPivot.SelectedIndex = 2;
        
   

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DatabaseHelperClass db = new DatabaseHelperClass();
                 //String Title,String Desc,String[] Hashtags,String Price,String Id,String SellerId,String SellerName,String Timestamp,String Type,String Distance
         
            OwnSalesList.ItemsSource =fetchOwnProducts(settings["userid"].ToString());
            ChatList.ItemsSource = fetchOwnChats();
            FavSalesList.ItemsSource = fetchFavProducts();

        }
   
        //List event controller
        private void OwnSalesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Get Currently selected, can be null.
            var selectedItem = (Product)OwnSalesList.SelectedItem;

            //Check if anything is selected.
            if(selectedItem == null){

                return;
            }
            PhoneApplicationService.Current.State["product"] = selectedItem;
            //Id of currently selected element
            var id = selectedItem.Id_;

            //Navigate to product page and pass ID of currently selected item to the product page
            //**********************TODO: OwnProductPage.xaml***************************
            NavigationService.Navigate(new Uri("/Views/OwnProductPage.xaml", UriKind.Relative));

            //Set currently selected item to null 
            //to avoid problems with going back to list and selecting same item (No itemChanged event)
            OwnSalesList.SelectedItem = null;
        }


   /*     public static readonly DependencyProperty PageNumberProperty =
           DependencyProperty.Register("PageNumber", typeof(int), typeof(FavoritesPage), new PropertyMetadata(0));
        /// <summary>
        /// Gets or sets the current page number
        /// </summary>
        /// <remarks>Setting this property has no effect in the demo app</remarks>
        public int PageNumber
        {
            get { return (int)GetValue(PageNumberProperty); }
            set { SetValue(PageNumberProperty, value); }
        }*/

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/MainScreen.xaml", UriKind.Relative));
            base.OnBackKeyPress(e);

        }
        BitmapImage convertFromBase64(string s)
        {
         
            byte[] fileBytes = Convert.FromBase64String(s);

            using (MemoryStream ms = new MemoryStream(fileBytes, 0, fileBytes.Length))
            {
                ms.Write(fileBytes, 0, fileBytes.Length);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(ms);
                return bitmapImage;
            }
        }
        private List<Product> fetchOwnProducts(string userID)
        {

            List<Product> ownProducts = new List<Product>();
            DatabaseHelperClass db = new DatabaseHelperClass();

            var query = db.getOwnProducts(userID);
            Debug.WriteLine(userID);
            foreach (var product in query)
            {
                Debug.WriteLine("product in own query");
                Product ownProduct = new Product(product.title_, product.desc_, null, product.price_, product.mongoId_, userID, product.username_, product.price_, "own", "0");
             
                var q = db.getFirstProductImage(product.mongoId_);
                if (q != null)
                {
           
                    ownProduct.previewImg_ = convertFromBase64(q.mData_);
                }
                else
                {
              
                    ownProduct.previewImg_ = new BitmapImage(new Uri(@"/Assets/canon.jpg", UriKind.RelativeOrAbsolute));
                }
    

                
                ownProducts.Add(ownProduct);
            }



            return ownProducts;
        }

  
        private List<Product> fetchFavProducts()
        {

            List<Product> favProducts = new List<Product>();
            DatabaseHelperClass db = new DatabaseHelperClass();

            var query = db.getFavProducts();
          
            foreach (var product in query)
            {

                Debug.WriteLine("product in query");
                Product favProduct = new Product(product.title_, product.desc_, product.tags_, product.price_, product.mongoId_, product.userId_, product.username_, product.price_, "fav", product.distance_);

                var q = db.getFirstProductImage(product.mongoId_);
                if (q != null)
                {

                    favProduct.previewImg_ = convertFromBase64(q.mData_);
                }
                else
                {

                    favProduct.previewImg_ = new BitmapImage(new Uri(@"/Assets/canon.jpg", UriKind.RelativeOrAbsolute));
                }
                favProducts.Add(favProduct);
            }



            return favProducts;
        }
        private  List<Chat> fetchOwnChats(){
            List<Chat> chats = new List<Chat>();
            DatabaseHelperClass db = new DatabaseHelperClass();

            var query = db.getMyChats(settings["userid"].ToString());

            foreach(var chat in query){
                Debug.WriteLine("chat");
                ChatTable newChat = new ChatTable(chat.senderId_, chat.receiverId_, chat.message_, chat.timestamp_, chat.chatPartnerName_, chat.mongoId_, chat.productId_);
                Chat chatItem = new Chat(chat.chatPartnerName_,chat.chatId_,"",null);
                var q = db.getFirstProductImage(chat.productId_);
                if(q != null)
                {
                   chatItem.previewImg_ = convertFromBase64(q.mData_);
                }
                else
                {
                     chatItem.previewImg_ = new BitmapImage(new Uri(@"/Assets/canon.jpg", UriKind.RelativeOrAbsolute));
                }
                var qTitle = db.getProduct(newChat.productId_);
                if (qTitle != null)
                {
                    chatItem.productName_ = qTitle.title_;
                    chatItem.productId_ = qTitle.mongoId_;
                }
                else
                {
                    newChat.productName_ = "no title";
                }

                chats.Add(chatItem);
            }


            return chats;
        }

    /*    private void ContentControl_SwipeLeft(object sender, EventArgs e)
        {
            Debug.WriteLine("Swipeleft");
            ++this.PageNumber;
            UpdateContentControl();
        }

        private void ContentControl_SwipeRight(object sender, EventArgs e)
        {
            --this.PageNumber;
            UpdateContentControl();
        }

        private void UpdateContentControl()
        {
            uxContentPanel.CanSwipeRight = (this.PageNumber > 0);
            uxContentPanel.CanSwipeLeft = (this.PageNumber < 2);
            if (PageNumber == 0)
            {
                NavigationService.Navigate(new Uri("/Views/MainScreen.xaml", UriKind.Relative));
            }
            else if (PageNumber == 1)
            {
                NavigationService.Navigate(new Uri("/Views/BrowseProducts.xaml", UriKind.Relative));
            }
            else if (PageNumber == 2)
            {
                NavigationService.Navigate(new Uri("/Views/FavoritesPage.xaml", UriKind.Relative));
            }

        }
        */
        private void FavSalesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Get Currently selected, can be null.
            var selectedItem = (Product)FavSalesList.SelectedItem;

            //Check if anything is selected.
            if (selectedItem == null)
            {

                return;
            }

            //Id of currently selected element
            var id = selectedItem.Id_;

            //Navigate to product page and pass ID of currently selected item to the product page
            //**********************TODO: OwnProductPage.xaml***************************
            NavigationService.Navigate(new Uri("/Views/ProductPage.xaml?id=" + id, UriKind.Relative));

            //Set currently selected item to null 
            //to avoid problems with going back to list and selecting same item (No itemChanged event)
            FavSalesList.SelectedItem = null;


        }

        private void ChatList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            //Get Currently selected, can be null.
            var selectedItem = (Chat)ChatList.SelectedItem;

            //Check if anything is selected.
            if (selectedItem == null)
            {

                return;
            }

            //Id of currently selected element
            DatabaseHelperClass db = new DatabaseHelperClass();

          ProductsTable query =  db.getProduct(selectedItem.productId_);
            // public Product(String Title,String Desc,String[] Hashtags,String Price,String Id,String SellerId,String SellerName,String Timestamp,String Type,String Distance){
          Product product = new Product(query.title_,query.desc_,"",query.price_,query.mongoId_,query.userId_,query.username_,query.timestamp_,query.type_,query.distance_);

          PhoneApplicationService.Current.State["product"] = product;

          var getChatTableObject = db.getChatObject(selectedItem.chatId_);
          PhoneApplicationService.Current.State["chat"] = getChatTableObject as ChatTable;
           // Debug.WriteLine("Lol" + id);
            //Navigate to product page and pass ID of currently selected item to the product page
    
             NavigationService.Navigate(new Uri("/Views/chatPage.xaml", UriKind.Relative));

            //Set currently selected item to null 
            //to avoid problems with going back to list and selecting same item (No itemChanged event)
            ChatList.SelectedItem = null;

        }

        private void FavPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            OwnTabLabel.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0)); 
            FavTabLabel.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0)); 
            ChatTabLabel.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            if (FavPivot.SelectedIndex == 0)
            {
                OwnTabLabel.Foreground = new SolidColorBrush(Color.FromArgb(255,75,161,196));
            }
            else if (FavPivot.SelectedIndex == 1)
            {
                FavTabLabel.Foreground = new SolidColorBrush(Color.FromArgb(255, 75, 161, 196)); 
            }
            else
            {
                ChatTabLabel.Foreground = new SolidColorBrush(Color.FromArgb(255, 75, 161, 196)); 
            }


        }

        private void ownLabel_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FavPivot.SelectedIndex = 0;
        }

        private void favLabel_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FavPivot.SelectedIndex = 1;
        }

        private void chatLabel_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FavPivot.SelectedIndex = 2;
        }

        private void addSale_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/addSalePage.xaml", UriKind.Relative));
        }
    }
}