﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading.Tasks;
using System.Windows.Media;
using System.IO;

namespace Baija_dev
{
    public partial class ProductPage : PhoneApplicationPage
    {
        double ScreenWidth;
        double ScreenHeight;
        int currentImage;
        bool isGrey = true;
        Product product = new Product();
 
        public ProductPage()
        {
            InitializeComponent();
            ScreenWidth = Application.Current.Host.Content.ActualWidth;
            ScreenHeight = Application.Current.Host.Content.ActualHeight;

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            product = PhoneApplicationService.Current.State["product"] as Product;

            productTitle.Text = product.Title_;
            productPrice.Text = product.Price_;
            Description_block.Text = product.Desc_;
            productDistance.Text = product.Distance_;
            string tags = "";
          
            productsHashtags.Text = tags;
     
            DatabaseHelperClass db = new DatabaseHelperClass();

           var query = db.getAllPicturesOfProduct(product.Id_);

           foreach (var image in query)
           {
               CreatePivotItem(image.mData_);
           }

        }

        private void P_Image_Changed(object sender, SelectionChangedEventArgs e)
        {
            currentImage = BrowseProductImages.SelectedIndex;
        }

        private void Remove_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MessageBoxResult mb = MessageBox.Show("Are you sure you want to remove this from listing?\n(You wont be able to find this item after removing)", "Confirm", MessageBoxButton.OKCancel);

            if (mb == MessageBoxResult.OK)
            {

                //TODO: RemoveFromListing(currentItem);

            }
        }

        private void Favorite_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FadeOut(1, 0, isGrey);
            //animateStar2(0, 1, isGrey);
            //Swap the boolean from true -> false or false -> true
            isGrey = !isGrey;



        }
        private void FadeOut(int From, int To, bool isGrey)
        {
            DoubleAnimation resize = new DoubleAnimation();
            resize.From = From;
            resize.To = To;
            resize.Duration = new Duration(TimeSpan.FromSeconds(0.5));

            var storyBoard = new Storyboard();
            Storyboard.SetTarget(resize, FavouriteStar);
            Storyboard.SetTargetProperty(resize, new PropertyPath("Opacity"));
            storyBoard.Children.Add(resize);
            storyBoard.Begin();
            storyBoard.Completed += (s, a) =>
            {
                if (isGrey)
                {
                    FavouriteStar.Source = new BitmapImage(new Uri(@"/Assets/star.png", UriKind.RelativeOrAbsolute));
                    fadeIn(0, 1, isGrey);
                }
                else
                {
                    FavouriteStar.Source = new BitmapImage(new Uri(@"/Assets/grey_star.png", UriKind.RelativeOrAbsolute));
                    fadeIn(0, 1, isGrey);
                }

            };

        }
        private void fadeIn(int From, int To, bool isGrey)
        {
            DoubleAnimation resize = new DoubleAnimation();
            resize.From = From;
            resize.To = To;
            resize.Duration = new Duration(TimeSpan.FromSeconds(0.5));

            var storyBoard = new Storyboard();
            Storyboard.SetTarget(resize, FavouriteStar);
            Storyboard.SetTargetProperty(resize, new PropertyPath("Opacity"));
            storyBoard.Children.Add(resize);
            storyBoard.Begin();
        }



        private void backButton_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {

            NavigationService.GoBack();

        }

        private void alertButton_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MessageBoxResult mb = MessageBox.Show("Are you sure you want to report this post as inappropriate/offensive", "Confirm", MessageBoxButton.OKCancel);

            if (mb == MessageBoxResult.OK)
            {

                NavigationService.Navigate(new Uri("/Views/reportProduct.xaml", UriKind.Relative));

            }

        }

        private async Task CreatePivotItem(string img64base)
        {
          
            PivotItem pivotItem = new PivotItem();
            Grid containerGrid = new Grid();

            Image templateImage = new Image();
            Rectangle roundedImage = new Rectangle();
            Rectangle imageDropShadow = new Rectangle();

            roundedImage.HorizontalAlignment = HorizontalAlignment.Center;
            imageDropShadow.HorizontalAlignment = HorizontalAlignment.Center;


            roundedImage.RadiusX = 35;
            roundedImage.RadiusY = 35;
            roundedImage.Width = ScreenWidth * 4 / 5;
            roundedImage.Height = ScreenWidth * 4 / 5;



            imageDropShadow.Width =( ScreenWidth * 4 / 5 );
            imageDropShadow.Height =( ScreenWidth * 4 / 5 );


            Thickness margin = imageDropShadow.Margin;
            margin.Left = 10;
            margin.Top = 10;
            imageDropShadow.Margin = margin;
            
            imageDropShadow.RadiusX = 35;
            imageDropShadow.RadiusY = 35;
            imageDropShadow.Opacity = 0.3;
            imageDropShadow.RenderTransformOrigin = new Point(0, 0);
            imageDropShadow.StrokeThickness = 16;
            imageDropShadow.StrokeDashCap = PenLineCap.Round;
            imageDropShadow.StrokeEndLineCap = PenLineCap.Round;
            imageDropShadow.StrokeLineJoin = PenLineJoin.Round;
            imageDropShadow.StrokeStartLineCap = PenLineCap.Round;
            imageDropShadow.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
          //  CompositeTransform scale = new CompositeTransform();
           // scale.ScaleX = 1.02;
          //  scale.ScaleY = 1.02;
          //  imageDropShadow.RenderTransform = scale;

            ImageBrush imageBrush = new ImageBrush();

            if (!img64base.Equals(""))
            {
                Debug.WriteLine("templateen base64");
                templateImage.Source = convertFromBase64(img64base);
                imageBrush.ImageSource = convertFromBase64(img64base);
            }
            else
            {
                templateImage.Source = new BitmapImage(new Uri(@"/Assets/canon.jpg", UriKind.RelativeOrAbsolute));
                imageBrush.ImageSource = new BitmapImage(new Uri(@"/Assets/canon.jpg", UriKind.RelativeOrAbsolute));
            }
            roundedImage.Fill = imageBrush;
            //   templateImage
            containerGrid.CacheMode = new BitmapCache();


            containerGrid.Children.Add(imageDropShadow);
            Grid.SetColumn(imageDropShadow, 0);
            Grid.SetColumnSpan(imageDropShadow, 2);
            Grid.SetRow(imageDropShadow, 0);

            containerGrid.Children.Add(roundedImage);
            Grid.SetColumn(roundedImage, 0);
            Grid.SetColumnSpan(roundedImage, 2);
            Grid.SetRow(roundedImage, 0);


            pivotItem.Content = containerGrid;

            BrowseProductImages.Items.Add(pivotItem);
            // BrowseSinglePivot.

        }

        BitmapImage convertFromBase64(string s)
        {
            Debug.WriteLine("converfrom" + s);
            byte[] fileBytes = Convert.FromBase64String(s);

            using (MemoryStream ms = new MemoryStream(fileBytes, 0, fileBytes.Length))
            {
                ms.Write(fileBytes, 0, fileBytes.Length);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(ms);
                return bitmapImage;
            }
        }

        private void OpenChat_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneApplicationService.Current.State["chat"] = null;
            NavigationService.Navigate(new Uri("/Views/chatPage.xaml",UriKind.Relative));
        }
    }
}