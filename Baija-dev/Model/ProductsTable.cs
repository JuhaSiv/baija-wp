﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baija_dev
{
    public class ProductsTable
    {
       // [SQLite.PrimaryKey, SQLite.AutoIncrement]
        //public int SqlId { get; set; }
        public string mongoId_ { get; set; }
        public string userId_ { get; set; }
        public string username_ { get; set; }
        public string title_ { get; set; }
        public string desc_ { get; set; }
        public string price_ { get; set; }
        public string timestamp_ { get; set; }
        public string tags_ { get; set; }
        public string distance_ { get; set; }
        public string type_ { get; set; }
        public string noteType_ { get; set; }
        
        public ProductsTable()
        {
            mongoId_ = "";
            userId_ = "";
            username_ = "";
            title_ = "";
            desc_ = "";
            price_ = "";
            timestamp_ = "";
            tags_ = "";
            distance_ = "";
            noteType_ = "";
            type_ = ""  ;




        }
        public ProductsTable(string mongoId,string userId,string username,string title,string desc,string price,string timestamp,string tags, string distance, string type,string noteType)
        {
            mongoId_ = mongoId;
            userId_ = userId;
            username_ = username;
            title_ = title;
            desc_ = desc;
            price_ = price;
            timestamp_ = timestamp;
            tags_ = tags;
            distance_ = distance;
            type_ = type;
            noteType_ = noteType;


        }


      
    }
}
