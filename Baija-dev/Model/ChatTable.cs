﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Baija_dev.dbClasses
{
   public class ChatTable
    {
      //  [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int SqlId { get; set; }
        public string senderId_ {get;set;}
        public string mongoId_ { get; set; }
        public string productId_ { get; set; }
        public string chatPartnerName_ { get; set; }
        public string receiverId_ { get; set; }
        public string message_ { get; set; }
        public string timestamp_ { get; set; }
        public string chatId_ { get; set; }
        public string productName_ { get; set; }
     //   public Product previewImg_ { get; set; }

       public ChatTable()
        {

        }

       public ChatTable(string senderId, string receiverId, string message, string timestamp, string chatPartnerName, string mongoId, string productId)
       {
           IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
           senderId_ = senderId;
           receiverId_ = receiverId;
           chatPartnerName_ = chatPartnerName;
           message_ = message;
           timestamp_ = timestamp;
           productId_ = productId;
           mongoId_ = mongoId;
           if (senderId.Equals(settings["userid"].ToString()))
           {
               chatId_ = productId + receiverId;
           }
           else 
           {
               chatId_ = productId + senderId;
           }
       //    previewImg_ = null;
           productName_ = null;
       }

    }
}
